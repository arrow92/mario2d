#include "fonctions.h"
int main()
{

lancerAllegro(1024,720);
t_cliff cliff;
t_fichier fichier;
int i;
int Mapx=0;
int Mapy=0;
int contact=0;
int nb[2];
int c_map=0;
int c_map_old=c_map;

btmp image;


image.buffer=create_bitmap(SCREEN_W,SCREEN_H);
image.buffer_persos=create_bitmap(SCREEN_W,SCREEN_H);
image.buffer_obst=create_bitmap(SCREEN_W, SCREEN_H);
image.buffer_obj=create_bitmap(SCREEN_W, SCREEN_H);

image.cliff_long=load_bitmap("textures/cliff_long.bmp", NULL);
image.rotation=create_bitmap(SCREEN_W,SCREEN_H);
image.background[0]=load_bitmap("textures/bckground_1_0.bmp", NULL);
image.background[1]=load_bitmap("textures/bckground_1_1.bmp", NULL);
//              char bckg[50];
//sprintf(bckg,"textures/background_%d.bmp", c_map);
//image.bckg=load_bitmap(bckg, NULL);
image.bckg=load_bitmap("textures/background_5.bmp", NULL);
image.buffer_bckg=create_bitmap(SCREEN_W,SCREEN_H);
image.cloud[1]=load_bitmap("textures/cloud.bmp",NULL);
image.cloud[0]=load_bitmap("textures/cloud.bmp",NULL);
image.boo[0]=load_bitmap("textures/boo.bmp",NULL);
image.boo[1]=load_bitmap("textures/boo_2.bmp",NULL);
image.boo[2]=load_bitmap("textures/boo_hide.bmp",NULL);
image.boo[3]=load_bitmap("textures/boo_hide_2.bmp",NULL);


image.buffer_enemi=create_bitmap(SCREEN_W,SCREEN_H);
image.pipe=load_bitmap("textures/pipe.bmp", NULL);
image.pipe_2=load_bitmap("textures/pipe_2.bmp", NULL);
image.pipe_3=load_bitmap("textures/pipe_3.bmp", NULL);
image.flag=load_bitmap("textures/flag.bmp", NULL);

image.buffer_map=create_bitmap(SCREEN_W/7, SCREEN_H/7);


image.perso[0]=load_bitmap("textures/mario_0_0.bmp", NULL);
image.perso[1]=load_bitmap("textures/mario_0_1.bmp", NULL);

image.perso[2]=load_bitmap("textures/mario_1_0.bmp", NULL);
image.perso[3]=load_bitmap("textures/mario_1_1.bmp", NULL);

image.perso[4]=load_bitmap("textures/mario_2_0.bmp", NULL);
image.perso[5]=load_bitmap("textures/mario_2_1.bmp", NULL);

image.perso[6]=load_bitmap("textures/mario_3_0.bmp", NULL);
image.perso[7]=load_bitmap("textures/mario_3_1.bmp", NULL);


for(i=0;i<5;i++)
{
   image.sprite_perso[i]=create_bitmap((float)image.perso[0]->w/4,(float)image.perso[0]->h/4);
}


image.title=load_bitmap("textures/title.bmp", NULL);
image.title_1=load_bitmap("textures/title_press.bmp", NULL);
image.block=load_bitmap("textures/block.bmp",NULL);
image.b_block=load_bitmap("textures/b_block.bmp",NULL);
image.piece=load_bitmap("textures/pickup_coin.bmp", NULL);
image.piece_b=load_bitmap("textures/pickup_coin_b.bmp", NULL);
image.piece_collision=create_bitmap(image.piece->w/20,image.piece->h);
image.voile=create_bitmap(SCREEN_W,SCREEN_H);
image.para=load_bitmap("textures/paratroopa.bmp", NULL);
image.goom=load_bitmap("textures/goomba.bmp", NULL);
image.fireball=load_bitmap("textures/fireball.bmp", NULL);
image.antig[0]=load_bitmap("textures/antig_0.bmp", NULL);
image.antig[1]=load_bitmap("textures/antig_1.bmp", NULL);



t_effect* effect;
effect=malloc(sizeof(t_effect));

effect->title=load_sample("sound/title.wav");
effect->level=NULL;
effect->coin=load_sample("sound/coin.wav");

effect->k_shell=load_sample("sound/k_shell.wav");
effect->level_end=load_sample("sound/level_end.wav");
effect->hurt[0]=load_sample("sound/hurt_00.wav");
effect->hurt[1]=load_sample("sound/hurt_01.wav");
effect->warp=load_sample("sound/warp.wav");
effect->fireball=load_sample("sound/fireball.wav");


t_editeur* ed=malloc(sizeof(t_editeur));

init_editeur(ed);


t_objet** pipe=malloc(ed->nb_pipes_max*sizeof(t_pipe*));
t_objet** bloc=malloc(ed->nb_blocks_max*sizeof(t_objet*));
t_objet** b_bloc=malloc(ed->nb_b_blocks_max*sizeof(t_objet*));
t_objet** para=malloc(ed->nb_para_max*sizeof(t_objet*));
t_objet** goom=malloc(ed->nb_goom_max*sizeof(t_objet*));
t_objet** nuage=malloc(ed->weather_max*sizeof(t_objet*));
 t_objet** boo=malloc(ed->nb_boo_max*sizeof(t_objet*));
 t_objet** piece=malloc(ed->nb_pieces_max*sizeof(t_objet*));
  t_objet** antig=malloc(ed->nb_antig_max*sizeof(t_objet*));
 //rajouter antigravité
 t_perso** perso=malloc(ed->nb_persos_max*sizeof(t_perso*));
  t_objet** fireball=malloc(ed->nb_persos_max*ed->nb_fire_max*sizeof(t_objet*));

c_map=-1;




 ed=ALLOCATION_MONDE(image,perso,pipe,bloc,b_bloc,para,goom,nuage,boo,piece,fireball,antig,fichier,effect,c_map);

 ed->c_map=c_map;
// play_sample(effect->title,255,128,1000,1); //title music
 menu(image,perso,pipe,bloc,b_bloc,para,goom,nuage,boo,piece,fichier,ed,effect);
stop_sample(effect->title);

ed=ALLOCATION_MONDE(image,perso,pipe,bloc,b_bloc,para,goom,nuage,boo,piece,fireball,antig,fichier,effect,0);

int x_arrivee=image.bckg->w-SCREEN_W/2;
    image.bckg=display_finish(image.flag,image.b_block,image.bckg,b_bloc,ed->nb_b_blocks_max,x_arrivee);
image=block_static(image,ed, b_bloc,image.b_block,ed->nb_b_blocks_max,c_map);
c_map=0;
ed->c_map=c_map;


int arrive=0;


creer_bloc(fireball,ed,ed->nb_persos_max*ed->nb_fire_max, 500,200);

do{

  clock_t timer=clock();


editeur_fps_adapt(ed);


tout_afficher_screen(image,ed,perso,bloc,b_bloc,para,goom,piece,nuage,boo,pipe,fireball,antig);


sprite_next_piece(piece,ed,ed->nb_pieces_max,ed->compteur_max,19);

sprite_next_piece(antig,ed,ed->nb_antig_max,ed->compteur_max,19);

sprite_next_piece(bloc,ed,ed->nb_blocks_max,3*ed->compteur_max,4);
sprite_next_piece(b_bloc,ed,ed->nb_b_blocks_max,10*ed->compteur_max,4);
sprite_next_piece(para,ed,ed->nb_para_max,ed->compteur_max,6);
sprite_next_piece(goom,ed,ed->nb_goom_max,2*ed->compteur_max,8);
sprite_next_piece(fireball,ed,ed->nb_persos_max*ed->nb_fire_max,ed->compteur_max,9);

detect_piece(perso,piece,ed,image,ed->nb_pieces_max,effect);

detect_piece(perso,antig,ed,image,ed->nb_antig_max,effect);

detect_bloc(perso, bloc,ed,image,effect,ed->nb_blocks_max,image.block->w/5,image.block->h);
detect_bloc(perso,para,ed,image,effect,ed->nb_para_max,image.para->w/7,image.para->h);
detect_bloc(perso,goom,ed,image,effect,ed->nb_goom_max,image.goom->w/9,image.goom->h);


gravite_objet(bloc,ed->fps_ratio,ed->nb_blocks_max);

deplacement_para(para,ed,image.para,ed->nb_para_max);
gravite_objet(para,ed->fps_ratio,ed->nb_para_max);


deplacement_goom(goom,ed,image.goom,image.bckg,9,ed->nb_goom_max);
gravite_objet(goom,ed->fps_ratio,ed->nb_goom_max);

//
//deplacement_para(piece,ed,image.para,ed->nb_pieces_max);
//deplacement_goom(piece,ed,image.piece,image.bckg,20,ed->nb_pieces_max);
coin_dep(piece,ed,image.piece,ed->nb_pieces_max);
gravite_objet(piece,ed->fps_ratio,ed->nb_pieces_max);


detect_bloc(perso,fireball,ed,image,effect,ed->nb_persos_max*ed->nb_fire_max,image.fireball->w/10,image.fireball->h);
rebond_fire(fireball,ed,image.bckg,image.fireball,10,ed->nb_persos_max*ed->nb_fire_max);
gravite_objet(fireball,ed->fps_ratio,ed->nb_persos_max*ed->nb_fire_max);

obj_obj_hostile(image,perso,fireball,para,image.para,image.fireball,ed,effect,10,7,ed->nb_para_max);
obj_obj_hostile(image,perso,fireball,goom,image.goom,image.fireball,ed,effect,10,9,ed->nb_goom_max);
obj_obj_hostile(image,perso,fireball,piece,image.piece,image.fireball,ed,effect,10,20,ed->nb_pieces_max);
obj_obj_hostile(image,perso,fireball,bloc,image.block,image.fireball,ed,effect,10,5,ed->nb_blocks_max);

obj_inter_cote(perso,boo, ed->nb_boo,ed->nb_persos_max);


  for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
          perso[i]->idle=1;

      }
  }


//printf("backup x:%d  y:%d  acc_y %f\n",(int)ed->backup[0]->x,(int)ed->backup[0]->y,ed->backup[0]->acc_y);


p_controls(ed);
for(i=0;i<ed->nb_persos_max;i++)
{
    if(perso[i]!=NULL)
    {
        if(perso[i]->cooldown_move<=0)
        {
JUMP(perso,ed, ed->tab[i][2],i);
GAUCHE_DROITE(perso,ed,image,ed->tab[i][0],ed->tab[i][1], i);
WARP(perso, ed,image,pipe,effect, ed->tab[i][3],i);
PUNCH(perso,fireball,effect, ed,image, ed->tab[i][4],i);

sprite_change( perso,i,ed->tab[i][5]);
    }
    }
    else{if (ed->tab[i][0])
       {
           if(ed->backup[i]!=NULL)
           {

               create_perso(perso,ed,i);
               printf("backup x:%d  y:%d\n",(int)ed->backup[i]->x,(int)ed->backup[i]->y);
               perso[i]->x=ed->backup[i]->x;
            perso[i]->y=ed->backup[i]->y;
//                perso[i]->taille=ed->backup[i]->taille;
perso[i]->etat=0;
                ed->scroll_x=ed->backup[i]->pieces;
                ed->acc_y=ed->backup[i]->acc_y;

//               ed->scroll_x=perso[i]->x-SCREEN_W/2;
           }
           else
          create_perso(perso,ed,i);
       }
    }
}



JUMP_beta(perso, ed, key[KEY_B],0);

camera_clic_move(ed,image.bckg);
life_check(perso,ed,effect);
cooldown_reduce_bloc(fireball, ed,ed->nb_persos_max*ed->nb_fire_max);

for(i=0;i<ed->nb_persos_max;i++)
{
    if(perso[i]!=NULL)
    {
collision_cote(perso,ed,image,i);
contact=0;
perso[i]->contact=contact_sol(perso,image, i,ed);


acceleration(perso, ed,i);
gravite(perso,i,ed,contact);
        if(perso[i]->idle==1)
        {
            perso[i]->vit_x/=1.5;
//image=perso_idle(image, perso,i);
        }
    }
}



printf("Map_%d\n", c_map);

if(key[KEY_ENTER])
{
  if(key[KEY_0_PAD]){
        ed->scroll_x=((int)ed->scroll_x/(image.block->w/5))*image.block->w/5;

  char n_bloc[50];

sprintf(n_bloc,"data/bloc/beta/bloc_map_%d.txt", c_map);
fichier.bloc=fopen(n_bloc,"w");

sprintf(n_bloc,"data/b_bloc/beta/bloc_map_%d.txt", c_map);
fichier.b_bloc=fopen(n_bloc,"w");

sprintf(n_bloc,"data/paratroopa/beta/bloc_map_%d.txt", c_map);
fichier.para=fopen(n_bloc,"w");

sprintf(n_bloc,"data/goomba/beta/bloc_map_%d.txt", c_map);
fichier.goom=fopen(n_bloc,"w");

sprintf(n_bloc,"data/pieces/beta/bloc_map_%d.txt", c_map);
fichier.pieces=fopen(n_bloc,"w");

sprintf(n_bloc,"data/pipes/beta/bloc_map_%d.txt", c_map);
fichier.pipes=fopen(n_bloc,"w");

sprintf(n_bloc,"data/antig/beta/bloc_map_%d.txt", c_map);
fichier.antig=fopen(n_bloc,"w");


show_mouse(screen);
do{
    tout_afficher_screen(image,ed,perso,bloc,b_bloc,para,goom,piece,nuage,boo,pipe,fireball,antig);
        carreaux(mouse_x,mouse_y, image.block->w/5, image.block->h);

if(key[KEY_C])
{

camera_clic_move(ed,image.bckg);
ed->scroll_x=((int)ed->scroll_x/(image.block->w/5))*image.block->w/5;
//rest(10);

}


if(key[KEY_T])
{
    fprintf(fichier.bloc, "%d %d 1\n",(mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(bloc,ed,ed->nb_blocks_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.block,screen,image.block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h );

rest(100);
}


if(key[KEY_R])
{
    fprintf(fichier.b_bloc, "%d %d 1\n", (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(b_bloc,ed,ed->nb_b_blocks_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.b_block,screen,image.b_block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h);

rest(100);
}

if(key[KEY_O])
{
    fprintf(fichier.para, "%d %d 42\n", (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(para,ed,ed->nb_para_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.b_block,screen,image.b_block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h);

rest(100);
}


if(key[KEY_G])
{
    fprintf(fichier.goom, "%d %d 42\n", (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(goom,ed,ed->nb_goom_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.b_block,screen,image.b_block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h);

rest(100);
}


if(key[KEY_M])
{
    fprintf(fichier.pieces, "%d %d 4\n",(mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(piece,ed,ed->nb_pieces_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.block,screen,image.block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h );

rest(100);
}

if(key[KEY_L])
{
    fprintf(fichier.pieces, "%d %d 40\n",(mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(piece,ed,ed->nb_pieces_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.block,screen,image.block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h );

rest(100);
}

if(key[KEY_H])
{
    fprintf(fichier.pipes, "%d %d\n",(mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(pipe,ed,ed->nb_pipes_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.block,screen,image.block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h );

rest(100);
}


if(key[KEY_4_PAD])
{
    fprintf(fichier.antig, "%d %d 20\n",(mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(antig,ed,ed->nb_antig_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.block,screen,image.block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h );

rest(100);
}

if(key[KEY_5_PAD])
{
    fprintf(fichier.antig, "%d %d 21\n",(mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
creer_bloc(antig,ed,ed->nb_antig_max, (mouse_x/(image.block->w/5))*(image.block->w/5)+(int)ed->scroll_x,(mouse_y/image.block->h)*image.block->h);
//      masked_blit(image.block,screen,image.block->w/5, 0,(mouse_x/(image.block->w/5))*(image.block->w/5),(mouse_y/image.block->h)*image.block->h,image.block->w/5,image.block->h );

rest(100);
}

      rest(10);
  }while(!key[KEY_B]);

fclose(fichier.bloc);
fclose(fichier.b_bloc);
fclose(fichier.para);
fclose(fichier.goom);
fclose(fichier.pieces);
fclose(fichier.pipes);
fclose(fichier.antig);

//I_editor++;
}
}

if(key[KEY_N])
{
 if(key[KEY_B])
 {
     ed->nb_boo=creer_boo(boo,ed->nb_boo_max,1);
//      ed->weather=creer_boo(nuage,ed->weather_max,1);
do{rest(1);}while(key[KEY_B]);
 }
 if(key[KEY_V])
 {
      ed->weather=creer_boo(nuage,ed->weather_max,0);
do{rest(1);}while(key[KEY_V]);
 }


if(key[KEY_T])
{
    if(ed->fps_max>10)
ed->fps_max-=10;

do{rest(1);}while(key[KEY_T]);
}

if(key[KEY_Y])
{
ed->fps_max+=10;
do{rest(1);}while(key[KEY_Y]);
}


if(key[KEY_C])
{
   switch(ed->cursor)
   {
       case 0:ed->cursor=1;break;
       case 1:ed->cursor=0;break;
   }
   do{rest(1);}while(key[KEY_C]);set_cooldown(perso,ed->perso_select,100);
}



if(key[KEY_P])
{

  if(c_map<10)c_map++;

    do{rest(1);}while(key[KEY_P]);
}


if(key[KEY_X])
{

 c_map=-1;

    do{rest(1);}while(key[KEY_P]);
}


if(key[KEY_R])
{
 char bckg[50];

image.bckg=load_bitmap("textures/background_5.bmp", NULL);

ed=LOAD_NEXT_MAP(image,perso,pipe,bloc,b_bloc,para,goom,nuage,boo,piece,fireball,antig,fichier,effect,c_map,ed);
ed->c_map=c_map;
x_arrivee=image.bckg->w-SCREEN_W/2;
    image.bckg=display_finish(image.flag,image.b_block,image.bckg,b_bloc,ed->nb_b_blocks_max,x_arrivee);

image=block_static(image,ed, b_bloc,image.b_block,ed->nb_b_blocks_max,c_map);


 arrive=0;
    do{rest(1);}while(key[KEY_R]);
}

if(key[KEY_G])
{
    ed->acc_y=-ed->acc_y;
    do{rest(1);}while(key[KEY_G]);
}



if(key[KEY_L]){


        if(perso[ed->perso_select]!=NULL)
        {
         if(perso[ed->perso_select]->etat<2)
         {

            etat_mario(perso,ed->perso_select,perso[ed->perso_select]->etat +1);
         }

        }

    do{rest(1);}while(key[KEY_L]);
}

if(key[KEY_K]){


     bonus(perso,ed,ed->perso_select,-2);

    do{rest(1);}while(key[KEY_K]);
}

if(key[KEY_5_PAD]){


     bonus(perso,ed,ed->perso_select,1);

    do{rest(1);}while(key[KEY_5_PAD]);
}
if(key[KEY_6_PAD]){


     bonus(perso,ed,ed->perso_select,-1);

    do{rest(1);}while(key[KEY_6_PAD]);
}

if(key[KEY_DOWN])
{
    etat_mario(perso,ed->perso_select,1);

}

if(key[KEY_UP])
{
    etat_mario(perso,ed->perso_select,2);

}

}

scrolling(ed,perso,image);
//collision_persos(perso, ed, image);
detect_dead(perso,ed,image);
if(arrive==0)arrive=arrivee(perso,ed, image, x_arrivee);
if(arrive==1){



  if(c_map<20)c_map++;


}


if(c_map_old!=c_map)
{

         stop_sample(effect->level);
 play_sample(effect->level_end,255,128,1000,0);
for(i=0;i<700;i++)
{
    clear_to_color(image.buffer,makecol(255,0,255));
  draw_sprite(image.buffer,image.buffer_persos,0,0);
cave(perso,ed,image.buffer,image.voile,image.sprite_perso[0],7 -(float)i/100);

draw_sprite(screen,image.buffer,0,0);

}

     char bckg[50];
//  if(c_map<10)c_map++;
//sprintf(bckg,"textures/background_%d.bmp", c_map);
//image.bckg=load_bitmap(bckg, NULL);
image.bckg=load_bitmap("textures/background_5.bmp", NULL);

ed=LOAD_NEXT_MAP(image,perso,pipe,bloc,b_bloc,para,goom,nuage,boo,piece,fireball,antig,fichier,effect,c_map,ed);
ed->c_map=c_map;

x_arrivee=image.bckg->w-SCREEN_W/2;
    image.bckg=display_finish(image.flag,image.b_block,image.bckg,b_bloc,ed->nb_b_blocks_max,x_arrivee);
image=block_static(image,ed, b_bloc,image.b_block,ed->nb_b_blocks_max,c_map);

 arrive=0;
 c_map_old=c_map;
}


cooldown_reduce(perso,ed,image);

c_map=ed->c_map;


do_sound(effect,ed,perso);


do{
   rest(1);

}
while(1/(((double)clock() - timer) / CLOCKS_PER_SEC)>ed->fps_max);



ed->fps=1/(((double)clock() - timer) / CLOCKS_PER_SEC);

if(ed->fps<10)ed->fps=60;

}while(!key[KEY_ESC]);



    return 0;
}
END_OF_MAIN();
