#include "fonctions.h"
int main()
{

lancerAllegro(1024,720);
t_cliff cliff;
t_fichier fichier;
int i;
int Mapx=0;
int Mapy=0;
int contact=0;
int nb[2];
int c_map=0;

btmp image;


image.buffer=create_bitmap(SCREEN_W,SCREEN_H);
image.buffer_persos=create_bitmap(SCREEN_W,SCREEN_H);
image.buffer_obst=create_bitmap(SCREEN_W, SCREEN_H);
image.cliff_long=load_bitmap("textures/cliff_long.bmp", NULL);
image.rotation=create_bitmap(SCREEN_W,SCREEN_H);
image.background[0]=load_bitmap("textures/bckground_1.bmp", NULL);
              char bckg[50];
sprintf(bckg,"textures/background_%d.bmp", c_map);
image.bckg=load_bitmap(bckg, NULL);

image.buffer_bckg=create_bitmap(SCREEN_W,SCREEN_H);
image.cloud[1]=load_bitmap("textures/cloud.bmp",NULL);
image.cloud[0]=load_bitmap("textures/cloud.bmp",NULL);
image.boo[0]=load_bitmap("textures/boo.bmp",NULL);
image.boo[1]=load_bitmap("textures/boo_2.bmp",NULL);
image.boo[2]=load_bitmap("textures/boo_hide.bmp",NULL);
image.boo[3]=load_bitmap("textures/boo_hide_2.bmp",NULL);


image.buffer_enemi=create_bitmap(SCREEN_W,SCREEN_H);
image.pipe=load_bitmap("textures/pipe.bmp", NULL);
image.pipe_2=load_bitmap("textures/pipe_2.bmp", NULL);
image.pipe_3=load_bitmap("textures/pipe_3.bmp", NULL);

image.buffer_map=create_bitmap(SCREEN_W/7, SCREEN_H/7);


image.perso=load_bitmap("textures/paper_mario.bmp", NULL);
for(i=0;i<5;i++)
{
   image.sprite_perso[i]=create_bitmap((float)image.perso->w/8,(float)image.perso->h/4);
}

image.block=load_bitmap("textures/block.bmp",NULL);
image.piece=load_bitmap("textures/pickup_coin.bmp", NULL);
image.piece_collision=create_bitmap(image.piece->w/20,image.piece->h);



t_editeur* ed=malloc(sizeof(t_editeur));

init_editeur(ed);


t_pipe** pipe=malloc(ed->nb_pipes_max*sizeof(t_pipe*));
t_objet** bloc=malloc(ed->nb_blocks_max*sizeof(t_objet*));
t_objet** nuage=malloc(ed->weather_max*sizeof(t_objet*));
 t_objet** boo=malloc(ed->nb_boo_max*sizeof(t_objet*));
 t_objet** piece=malloc(ed->nb_pieces_max*sizeof(t_objet*));
 t_perso** perso=malloc(ed->nb_persos_max*sizeof(t_perso*));

ed=ALLOCATION_MONDE(image,perso,pipe,bloc,nuage,boo,piece,fichier,1);


int x_arrivee=image.bckg->w-SCREEN_W/2;
int arrive=0;

do{

  clock_t timer=clock();


editeur_fps_adapt(ed);


tout_afficher_screen(image,ed,perso,bloc,piece,nuage,boo,pipe);
sprite_next_piece(piece,ed);
detect_piece(perso,piece,ed,image);
detect_bloc(perso, bloc,ed,image);
obj_inter_cote(perso,boo, ed->nb_boo,ed->nb_persos_max);


  for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
          perso[i]->idle=1;

      }
  }





p_controls(ed);
for(i=0;i<ed->nb_persos_max;i++)
{
    if(perso[i]!=NULL)
    {
JUMP(perso,ed, ed->tab[i][2],i);
GAUCHE_DROITE(perso,ed,image,ed->tab[i][0],ed->tab[i][1], i);
WARP(perso, ed,image,pipe, ed->tab[i][3],i);
PUNCH(perso, ed,image, ed->tab[i][4],i);
    }
    else{if (ed->tab[i][0]) create_perso(perso,ed,i);}
}



camera_clic_move(ed,image.bckg);



for(i=0;i<ed->nb_persos_max;i++)
{
    if(perso[i]!=NULL)
    {
collision_cote(perso,ed,image,i);
contact=0;
perso[i]->contact=contact_sol(perso,image, i,ed);


acceleration(perso, ed,i);
gravite(perso,i,ed,contact);
        if(perso[i]->idle==1)
        {
            perso[i]->vit_x/=1.5;
image=perso_idle(image, perso,i);
        }
    }
}



if(key[KEY_ENTER])
{

  char n_falaise[50];
sprintf(n_falaise,"levels_tests/falaise_level_%d_%d_test.txt", Mapx,Mapy);
fichier.falaise_write=fopen(n_falaise,"w");


show_mouse(screen);
do{





if(key[KEY_T])
{
    fprintf(fichier.falaise_write, "%d %d 192\n", mouse_x-image.cliff_long->w/2+image.cliff_long->h/2,mouse_y+image.cliff_long->w/2-image.cliff_long->h/2);
              char bckg[50];
sprintf(bckg,"textures/background_%d.bmp", c_map);
image.bckg=load_bitmap(bckg, NULL);
      clear_to_color(image.rotation,makecol(255,0,255));
    rotate_scaled_sprite(screen,image.cliff_long,mouse_x-image.cliff_long->w/2+image.cliff_long->h/2,mouse_y+image.cliff_long->w/2-image.cliff_long->h/2,ftofix(192),ftofix(1));

rest(100);
}

if(key[KEY_G])
{
    fprintf(fichier.falaise_write, "%d %d 64\n", mouse_x-image.cliff_long->w/2-image.cliff_long->h/2,mouse_y+image.cliff_long->w/2-image.cliff_long->h/2);

      clear_to_color(image.rotation,makecol(255,0,255));
    rotate_scaled_sprite(screen,image.cliff_long,mouse_x-image.cliff_long->w/2-image.cliff_long->h/2,mouse_y+image.cliff_long->w/2-image.cliff_long->h/2,ftofix(64),ftofix(1));

rest(100);
}

if(key[KEY_R])
{
    fprintf(fichier.falaise_write, "%d %d 0\n", mouse_x,mouse_y);

      clear_to_color(image.rotation,makecol(255,0,255));
    rotate_scaled_sprite(screen,image.cliff_long,mouse_x,mouse_y,ftofix(0),ftofix(1));

rest(100);
}


if(key[KEY_F])
{
    fprintf(fichier.falaise_write, "%d %d 128\n", mouse_x,mouse_y);

      clear_to_color(image.rotation,makecol(255,0,255));
    rotate_scaled_sprite(screen,image.cliff_long,mouse_x,mouse_y,ftofix(128),ftofix(1));


rest(100);
}





      rest(10);
  }while(!key[KEY_B]);

fclose(fichier.falaise_write);

//I_editor++;
}

if(key[KEY_N])
{
 if(key[KEY_B])
 {
     ed->nb_boo=creer_boo(boo,ed->nb_boo_max,1);
//      ed->weather=creer_boo(nuage,ed->weather_max,1);
do{rest(1);}while(key[KEY_B]);
 }
 if(key[KEY_V])
 {
      ed->weather=creer_boo(nuage,ed->weather_max,0);
do{rest(1);}while(key[KEY_V]);
 }


if(key[KEY_T])
{
    if(ed->fps_max>10)
ed->fps_max-=10;

do{rest(1);}while(key[KEY_T]);
}

if(key[KEY_Y])
{
ed->fps_max+=10;
do{rest(1);}while(key[KEY_Y]);
}


if(key[KEY_C])
{
   switch(ed->cursor)
   {
       case 0:ed->cursor=1;break;
       case 1:ed->cursor=0;break;
   }
   do{rest(1);}while(key[KEY_C]);
}



if(key[KEY_P])
{
 char bckg[50];
  if(c_map<10)c_map++;
sprintf(bckg,"textures/background_%d.bmp", c_map);
image.bckg=load_bitmap(bckg, NULL);


ed=LOAD_NEXT_MAP(image,perso,pipe,bloc,nuage,boo,piece,fichier,c_map,ed);

    x_arrivee=image.bckg->w-SCREEN_W/2;
 arrive=0;
    do{rest(1);}while(key[KEY_P]);
}


if(key[KEY_R])
{
 char bckg[50];
sprintf(bckg,"textures/background_%d.bmp", c_map);
image.bckg=load_bitmap(bckg, NULL);

ed=LOAD_NEXT_MAP(image,perso,pipe,bloc,nuage,boo,piece,fichier,c_map,ed);

    x_arrivee=image.bckg->w-SCREEN_W/2;
 arrive=0;
    do{rest(1);}while(key[KEY_P]);
}
}

scrolling(ed,perso,image);
//collision_persos(perso, ed, image);
detect_dead(perso,ed,image);
if(arrive==0)arrive=arrivee(perso,ed, image, x_arrivee);
if(arrive==1){

 char bckg[50];
  if(c_map<10)c_map++;
sprintf(bckg,"textures/background_%d.bmp", c_map);
image.bckg=load_bitmap(bckg, NULL);


ed=LOAD_NEXT_MAP(image,perso,pipe,bloc,nuage,boo,piece,fichier,c_map,ed);

    x_arrivee=image.bckg->w-SCREEN_W/2;
 arrive=0;
    //chgmt de map...

}
cooldown_reduce(perso,ed,image);



do{
   rest(1);

}
while(1/(((double)clock() - timer) / CLOCKS_PER_SEC)>ed->fps_max);



ed->fps=1/(((double)clock() - timer) / CLOCKS_PER_SEC);


}while(!key[KEY_ESC]);



    return 0;
}
END_OF_MAIN();
