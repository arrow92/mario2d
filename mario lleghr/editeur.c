#include "fonctions.h"




void init_editeur(t_editeur* ed)
{
    ed->nb_persos_max=5;
    ed->perso_select=0;
    ed->acc_x=(3*(float)SCREEN_W/50000);
//    ed->acc_y=2*(float)SCREEN_H/2500;
ed->acc_y=9.81*40/((float)SCREEN_H*6);
    ed->sprite_changed=0;
    ed->life_max=100;
    ed->vit_x_max=3;
    ed->vit_y_max=4;
    ed->scroll_x=0;
    ed->scroll_x_old=0;
    ed->nb_blocks_max=20;
    ed->nb_pieces_max=20;
    ed->compteur_max=5;
    ed->pieces=0;
    ed->weather=5;
    ed->weather_max=50;
    ed->nb_boo=0;
    ed->nb_boo_max=5;
    ed->nb_pipes_max=5;
    ed->fps_max=70;
    ed->fps_ori=100;
    ed->fps=60;
    ed->key_cooldown=0;
    ed->fps_ratio=1;
    ed->show_map=0;
    p_controls(ed);
    ed->cursor=1;
//    ed->fps=0;

}


void create_perso(t_perso** perso, t_editeur* ed,int i)
{
  int g;

   if(perso[i]!=NULL)
   {

   }
   else{
    perso[i]=malloc(sizeof(t_perso));
     perso[i]->x=SCREEN_W/2;
     perso[i]->y=SCREEN_H/2;
     perso[i]->vit_x=0;
     perso[i]->vit_y=0;
     perso[i]->idle=1;
     perso[i]->land=1;
//     perso[i]->poids=3*(float)SCREEN_H/(5000*18);
perso[i]->poids=20;
     perso[i]->acc_x=0;
     perso[i]->acc_y=0;
     perso[i]->life=100;
     perso[i]->pieces=0;
     perso[i]->cooldown=0;
     perso[i]->cooldown_sprite=0;
     perso[i]->action=0;
     perso[i]->taille=40;
     perso[i]->contact=0;
     perso[i]->direction=1;
     perso[i]->butt=0;
     for(g=0;g<360;g++)perso[i]->deg[g]=0;


//     perso[i]->couleur=makecol(100,100,100);
     switch(i)
     {
        case 0: perso[i]->couleur=makecol(0,255,0);break;
        case 1: perso[i]->couleur=makecol(0,100,255);break;
         case 2:perso[i]->couleur=makecol(100,255,0);break;
         case 3:perso[i]->couleur=makecol(100,0,100);break;
         case 4:perso[i]->couleur=makecol(100,0,255);break;

         default:perso[i]->couleur=makecol(100,100,100);break;
     }

   }






}



void create_piece(t_objet** piece,t_editeur* ed, btmp image)
{
    int i;
    for(i=0;i<ed->nb_pieces_max;i++)
    {
        piece[i]=malloc(sizeof(t_objet));
            piece[i]->x=rand()%image.bckg->w;
            piece[i]->y=rand()%(6*SCREEN_H/8)+SCREEN_H/8;
            piece[i]->sprite_etat=0;
            piece[i]->compteur=ed->compteur_max;
            piece[i]->bonus=1;
//            printf("X: %f,   Y: %f\n",piece[i]->x,piece[i]->y );

    }


}



int creer_boo(t_objet** boo, int nb_max,int interactif)
{
  int i,j;

for(i=0;i<nb_max;i++)
{
  if(boo[i]!=NULL)
  {
j=i;
  }
  else{
        boo[i]=malloc(sizeof(t_objet));
     boo[i]->compteur=rand()%SCREEN_W;
     boo[i]->y=0;
     boo[i]->cs=rand()%360;
     boo[i]->vit_y=rand()%100;
     boo[i]->vit_x=rand()%100;
     boo[i]->interactif=interactif;
    return i;
  }
}

return j;
   }


void editeur_fps_adapt(t_editeur* ed)
{
    ed->fps_ratio=ed->fps_ori/ed->fps;

}


void p_controls(t_editeur* ed)
{
    ed->tab[0][0]=key[KEY_LEFT];
    ed->tab[0][1]=key[KEY_RIGHT];
    ed->tab[0][2]=key[KEY_UP];
    ed->tab[0][3]=key[KEY_DOWN];
    ed->tab[0][4]=key[KEY_0_PAD];

     ed->tab[1][0]=key[KEY_A];
    ed->tab[1][1]=key[KEY_D];
    ed->tab[1][2]=key[KEY_W];
    ed->tab[1][3]=key[KEY_S];
      ed->tab[1][4]=key[KEY_E];

     ed->tab[2][0]=key[KEY_J];
    ed->tab[2][1]=key[KEY_L];
    ed->tab[2][2]=key[KEY_I];
    ed->tab[2][3]=key[KEY_K];
      ed->tab[2][4]=key[KEY_O];

    ed->tab[3][0]=key[KEY_F];
    ed->tab[3][1]=key[KEY_H];
    ed->tab[3][2]=key[KEY_T];
    ed->tab[3][3]=key[KEY_G];
      ed->tab[3][4]=key[KEY_Y];

    ed->tab[4][0]=key[KEY_4_PAD];
    ed->tab[4][1]=key[KEY_6_PAD];
    ed->tab[4][2]=key[KEY_8_PAD];
    ed->tab[4][3]=key[KEY_5_PAD];
      ed->tab[4][4]=key[KEY_9_PAD];

}



t_editeur* ALLOCATION_MONDE(btmp image,t_perso** perso,t_pipe** pipe,t_objet** bloc,t_objet** nuage,t_objet** boo,t_objet** piece,t_fichier fichier, int c_map)
{
    int i;





      t_editeur* ed=malloc(sizeof(t_editeur));
      init_editeur(ed);


    ////////////////////////////////////////////////////////////////
//////////alloc des pipes
////////////////////////////////////////////////
////////////////////////////////////////////////
for(i=0;i<ed->nb_pipes_max;i++)
{
    pipe[i]=malloc(sizeof(t_pipe));
    pipe[i]=NULL;
}

creer_pipe(pipe, ed, 100,200, 500,300);
creer_pipe(pipe, ed, 0,SCREEN_H-image.pipe->h, image.bckg->w-10*image.pipe->w,SCREEN_H-image.pipe->h);


////////Alloc des blocs
for(i=0;i<ed->nb_blocks_max;i++)
{
    bloc[i]=malloc(sizeof(t_objet));
    bloc[i]=NULL;
}
charger_blocs(bloc,ed, fichier);


////////Alloc des nuages

    for(i=0;i<ed->weather_max;i++)
   {
     nuage[i]=malloc(sizeof(t_objet));
     nuage[i]=NULL;
   }

   for(i=0;i<ed->weather;i++)
   {
     nuage[i]=malloc(sizeof(t_objet));
     nuage[i]->compteur=rand()%SCREEN_W;
     nuage[i]->y=0;
     nuage[i]->cs=rand()%360;
     nuage[i]->vit_y=rand()%100;
     nuage[i]->vit_x=rand()%100;
     nuage[i]->interactif=0;
   }



ini_nuages(nuage,ed);



//////////Alloc des boos

    for(i=0;i<ed->nb_boo_max;i++)
   {
     boo[i]=malloc(sizeof(t_objet));
     boo[i]=NULL;
   }

   for(i=0;i<ed->nb_boo;i++)
   {
     boo[i]=malloc(sizeof(t_objet));
     boo[i]->compteur=rand()%SCREEN_W;
     boo[i]->y=0;
     boo[i]->cs=rand()%360;
     boo[i]->vit_y=rand()%100;
     boo[i]->vit_x=rand()%100;
     boo[i]->interactif=1;
     boo[i]->sprite_etat=0;
   }



//////////Alloc des pieces
for(i=0;i<ed->nb_pieces_max;i++)
{
    piece[i]=malloc(sizeof(t_objet));
    piece[i]=NULL;

}
create_piece(piece,ed,image);

////////////////Alloc des persos

for(i=0;i<ed->nb_persos_max;i++)
{
    perso[i]=malloc(sizeof(t_perso));
    perso[i]=NULL;
}


return ed;
}
