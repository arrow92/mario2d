#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED


#include <allegro.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



#define NBMAX_PRIMIVE 100


typedef struct prim
{

}t_primitive;


typedef struct personnage
{
    float x;
    float y;
    float acc_x;
    float acc_y;
    float vit_x;
    float vit_y;
    float poids;
    int direction;
    int idle;
    int land;
    float life;
    int pieces;
    float cooldown;
    float cooldown_sprite;
    int action;
    float taille;
    int contact;
    int deg[361];
    int butt;
    int couleur;

}t_perso;



typedef struct image
{
BITMAP* buffer;
BITMAP* buffer_persos;

BITMAP* sprite_perso[5];
BITMAP* perso;
BITMAP* buffer_obst;
BITMAP* cliff_long;
BITMAP* rotation;
BITMAP* background[5];
BITMAP* bckg;
BITMAP* buffer_bckg;
BITMAP* buffer_enemi;
BITMAP* block;
BITMAP* piece;
BITMAP* piece_collision;
BITMAP* cloud[5];
BITMAP* boo[5];
BITMAP* pipe;
BITMAP* pipe_2;
BITMAP* pipe_3;
BITMAP* buffer_map;

}btmp;

typedef struct fichiers
{
FILE* save;
FILE* falaise;
FILE* falaise_write;
FILE* bloc;
}t_fichier;


typedef struct editeur
{
int nb_persos_max;
int perso_select;
float acc_x;
float acc_y;
int sprite_changed;
int life_max;
float vit_x_max;
float vit_y_max;
float scroll_x;
float scroll_x_old;
int nb_blocks_max;
int nb_pieces_max;
float compteur_max;
int pieces;
int weather;
int weather_max;
int nb_boo;
int nb_boo_max;
int nb_pipes_max;
float fps_max;
float fps;
float fps_ori;
float fps_ratio;
float key_cooldown;
int show_map;
char tab[5][5];
int cursor;
}t_editeur;


typedef struct cliffs
{
    int x[100];
    int y[100];
    float rot[100];
    int nombre;
}t_cliff;

typedef struct obj
{
    float x;
    float y;
    int bonus;
    int sprite_etat;
    float compteur;
    float cs;
    float vit_y;
    float vit_x;
    int interactif;
    int cote;
    int y_cot;

}t_objet;



typedef struct warp
{
    float x_1;
    float y_1;
    float x_2;
    float y_2;
}t_pipe;


#include "fonctionsalleg.h"

void init_editeur(t_editeur* ed);
void create_perso(t_perso** perso, t_editeur* ed,int i);
btmp sprite_change(t_perso** perso, t_editeur* ed,btmp image);
void tout_afficher_screen(btmp image,t_editeur* ed,t_perso** perso,t_objet** bloc,t_objet** piece,t_objet** nuage,t_objet** boo,t_pipe** pipe);
BITMAP* blit_perso(BITMAP* sprite, BITMAP* source, int dec_x,int dec_y);
btmp perso_idle(btmp image, t_perso** perso,int i);
void gravite(t_perso** perso,int i,t_editeur* ed,int contact);
void contact(t_perso** perso, t_editeur* ed, btmp image, int i, int j);
void detect_sol(t_perso** perso, btmp image,t_editeur* ed);
btmp charge_obstacle(btmp image,BITMAP* bmp, BITMAP* pic, float x, float y, int couleur, int draw);
int contact_sol(t_perso** perso,btmp image,int i,t_editeur* ed);
btmp charge_obstacle_turned(btmp image,BITMAP* bmp, BITMAP* pic,BITMAP* temp, float x, float y,int h,int w,float rotation);
FILE* charger_fichier(FILE* file, char* chaine);
BITMAP* barre(BITMAP* buffer,int x,int y, int couleur, t_perso** perso, int i, t_editeur* ed);
BITMAP* barre_de_vie(t_perso** perso,t_editeur* ed, BITMAP* buffer);
void punch(t_perso** perso,t_editeur* ed, btmp image);
int distance_contact(btmp image,BITMAP* obj_1,BITMAP* obj_2,float x_1,float y_1,float x_2,float y_2, float rayon);
void detect_dead(t_perso** perso, t_editeur* ed,btmp image);
void collision_persos(t_perso** perso, t_editeur* ed, btmp image);
void collision_cote(t_perso** perso, t_editeur* ed,btmp image,int i);

void scrolling(t_editeur* ed,t_perso** perso,btmp image);
BITMAP* background_scroll(btmp image, t_editeur* ed);
btmp charge_obstacle_intel(btmp image,BITMAP* bmp, BITMAP* pic,t_perso** perso, t_editeur* ed, float x, float y, int couleur, int draw);
btmp block(btmp image, t_editeur* ed, t_objet** bloc);
void create_piece(t_objet** piece,t_editeur* ed, btmp image);
BITMAP* pieces_afficher(BITMAP* piecee, BITMAP* buffer,t_perso** perso,t_editeur* ed,t_objet** piece);
void detect_piece(t_perso** perso, t_objet** piece,t_editeur* ed,btmp image);
void detect_bloc(t_perso** perso, t_objet** bloc,t_editeur* ed,btmp image);

BITMAP* beta_coord(t_perso** perso,t_editeur* ed, BITMAP* buffer);
void bonus(t_perso** perso, t_editeur* ed,int bonus);
BITMAP* afficher_nb_pieces(BITMAP* buffer, BITMAP* image, int pieces);
void ini_nuages(t_objet**  nuage,t_editeur* ed);
BITMAP* moving_clouds(t_objet** nuage, BITMAP* buffer,BITMAP* cloud[2],float scroll_x, int nb, float r);
void obj_inter_cote(t_perso** perso, t_objet** nuage, int nb, int nb_persos);
int creer_boo(t_objet** objet, int nb,int interactif);
BITMAP* affiche_boo(t_objet** boo,int nb, BITMAP* cloud[5], BITMAP* buffer);
void creer_pipe(t_pipe** pipe, t_editeur* ed, float x_1,float y_1, float x_2,float y_2);
btmp affiche_pipes(t_pipe** pipe,t_editeur* ed,btmp image,t_perso** perso);
void take_warp(t_perso** perso,t_editeur* ed, t_pipe** pipe,btmp image);
void editeur_fps_adapt(t_editeur* ed);
void cooldown_reduce(t_perso** perso, t_editeur* ed, btmp image);
void set_cooldown(t_perso** perso,int i, float time);
void camera_clic_move(t_editeur* ed, BITMAP* mape);
void show_map_advancement(BITMAP* buffer,float scroll, int width);
void set_cooldown_sprite(t_perso** perso,int i, float time);
void beta_fly(t_perso**perso,t_editeur* ed,int dir);
void acceleration(t_perso** perso, t_editeur* ed,int i);
int* angle_moyen(t_perso** perso, float g, btmp image,int tab[3], int i,t_editeur* ed);
void JUMP(t_perso** perso, t_editeur* ed, char k_jump,int sel);
void GAUCHE_DROITE(t_perso** perso,t_editeur* ed,btmp image,char left,char right, int sel);
void WARP(t_perso** perso, t_editeur* ed,btmp image, t_pipe** pipe, char down, int sel);
void PUNCH(t_perso** perso, t_editeur* ed,btmp image, char p,int sel);
BITMAP* cursor(BITMAP* buffer,BITMAP* sprite,t_perso** perso, t_editeur* ed);
int arrivee(t_perso** perso, t_editeur* ed, btmp image, int x_arrivee);
int check_arrivee(t_perso** perso,t_editeur* ed, btmp image, int check);
t_editeur* ALLOCATION_MONDE(btmp image,t_perso** perso,t_pipe** pipe,t_objet** bloc,t_objet** nuage,t_objet** boo,t_objet** piece,t_fichier fichier,int c_map);
t_editeur* LOAD_NEXT_MAP(btmp image,t_perso** perso,t_pipe** pipe,t_objet** bloc,t_objet** nuage,t_objet** boo,t_objet** piece,t_fichier fichier, int c_map,t_editeur* ed);

#endif // FONCTIONS_H_INCLUDED
