#include "fonctions.h"





btmp affiche_persos(btmp image, t_perso** perso, t_editeur* ed)
{
   int i;


   for(i=0;i<ed->nb_persos_max;i++)
   {
  if(perso[i]!=NULL)
  {

   draw_sprite(image.buffer_persos,image.sprite_perso[i],perso[i]->x,perso[i]->y);

  }


   }


    return image;
}




btmp sprite_change(t_perso** perso, t_editeur* ed,btmp image)
{
    int dec_x=0;
    int dec_y=0;



    if(perso[ed->perso_select]!=NULL)
    {

          clear_to_color(image.sprite_perso[ed->perso_select],makecol(255,0,255));
    switch(perso[ed->perso_select]->direction)
    {
    case -1:dec_x=6;
         break;
    case 1:dec_x=1;
        break;
    }


if(perso[ed->perso_select]->cooldown_sprite<=0)
        {
    if(perso[ed->perso_select]->action==1)
    {

         set_cooldown_sprite(perso,ed->perso_select, 50);
          perso[ed->perso_select]->action=2;
    }
        }

        if(perso[ed->perso_select]->action==2)
        {
            dec_y=2;
            switch(perso[ed->perso_select]->direction)
    {
    case -1:dec_x=0;
         break;
    case 1:dec_x=7;
        break;
    }
        }
    //un petit mario fait 175 par 178  (cadre)

  image.sprite_perso[ed->perso_select]=blit_perso(image.sprite_perso[ed->perso_select], image.perso,dec_x,dec_y);

    }
    return image;
}

//btmp
btmp perso_idle(btmp image, t_perso** perso,int i)
{
   int dec_x=0;
   int dec_y=0;

if(perso[i]!=NULL)
{

 clear_to_color(image.sprite_perso[i],makecol(255,0,255));
    if(perso[i]->direction==1)dec_x=7;
     if(perso[i]->direction==-1)dec_x=0;

   image.sprite_perso[i]=blit_perso(image.sprite_perso[i], image.perso, dec_x,dec_y);
}



    return image;
}

btmp border_draw(t_perso** perso, btmp image,t_editeur* ed)
{
blit(image.bckg, image.buffer_bckg, ed->scroll_x, 0, 0,0,SCREEN_W,SCREEN_H);

image=charge_obstacle_intel(image,image.buffer_obst, image.buffer_bckg,perso,ed,0,0 ,makecol(255,0,0), 1);
    return image;
}


btmp block(btmp image, t_editeur* ed, t_objet** bloc)
{
int i;
for(i=0;i<ed->nb_blocks_max;i++)
{
    if(bloc[i]!=NULL)
    {
  image=charge_obstacle(image,image.buffer_obst, image.block,bloc[i]->x-ed->scroll_x, bloc[i]->y, makecol(255,0,0), 1);


    }
}


return image;
}


BITMAP* blit_perso(BITMAP* sprite, BITMAP* source, int dec_x,int dec_y)
{
    masked_blit(source, sprite,(source->w/8)*dec_x,(source->h/4)*dec_y,0,0,(source->w/8),178);
return sprite;
}

void tout_afficher_screen(btmp image,t_editeur* ed,t_perso** perso,t_objet** bloc,t_objet** piece,t_objet** nuage,t_objet** boo,t_pipe** pipe)
{
    clear_to_color(image.buffer_obst,makecol(255,0,255));
    clear_to_color(image.buffer,makecol(255,0,255));
    clear_to_color(image.buffer_map,makecol(255,0,255));
     clear_to_color(image.buffer_persos,makecol(255,0,255));
      clear_to_color(image.buffer_enemi,makecol(255,0,255));



    image.buffer=background_scroll(image,ed);
      image.buffer=moving_clouds(nuage, image.buffer,image.cloud,ed->scroll_x,ed->weather_max, ed->fps_ratio);
      image.buffer_enemi=affiche_boo(boo,ed->nb_boo_max,image.boo, image.buffer_obst);
      image=charge_obstacle_intel(image,image.buffer_obst, image.buffer_enemi,perso,ed,0,0 ,makecol(0,255,0), 0);

image=charge_obstacle(image,image.buffer_obst, image.pipe_2,2*SCREEN_W-ed->scroll_x,SCREEN_H/2 ,makecol(0,100,0), 1);
image=charge_obstacle(image,image.buffer_obst, image.pipe_3,2*SCREEN_W-ed->scroll_x,0 ,makecol(0,110,0), 1);


    image=border_draw(perso,image,ed);
image=affiche_pipes(pipe,ed, image,perso);
    image=block(image,ed, bloc);

    image.buffer=moving_clouds(boo, image.buffer,image.boo,ed->scroll_x,ed->nb_boo_max,ed->fps_ratio);
   image.buffer=pieces_afficher(image.piece,image.buffer,perso,ed,piece);


   if(ed->show_map==1) {show_map_advancement(image.buffer_map,ed->scroll_x, image.bckg->w);ed->show_map=0;
draw_sprite(image.buffer,image.buffer_map,SCREEN_W/2,SCREEN_H/2);}

   image=affiche_persos(image,perso, ed);

image.buffer=barre_de_vie(perso,ed, image.buffer);
image.buffer=afficher_nb_pieces(image.buffer, image.piece,ed->pieces);
if(ed->cursor==1)image.buffer=cursor(image.buffer,image.sprite_perso[0],perso,ed);
image.buffer=beta_coord(perso,ed,image.buffer);
//
//draw_sprite(image.buffer,image.buffer_obst,0,0); //rrouge
    draw_sprite(image.buffer,image.buffer_persos,0,0);

    draw_sprite(screen,image.buffer,0,0);
}



btmp charge_obstacle_turned(btmp image,BITMAP* bmp, BITMAP* pic,BITMAP* temp, float x, float y,int h,int w,float rotation)
{
 int i,j;
 int c;
rotate_scaled_sprite(bmp,pic,x,y,ftofix(rotation),ftofix(1));
rotate_scaled_sprite(temp,pic,x,y,ftofix(rotation),ftofix(1));


 for(i=0;i<w;i++)
 {
     for(j=0;j<h;j++)
     {
        if(getpixel(temp,i,j)!=makecol(255,0,255))
        {
            putpixel(image.buffer_obst,i,j,makecol(255,0,0));

        }

     }

 }




 return image;
}


BITMAP* barre_de_vie(t_perso** perso,t_editeur* ed, BITMAP* buffer)
{
   int i;
   int x,y;
   int couleur;
   for(i=0;i<ed->nb_persos_max;i++)
   {
       if(perso[i]!=NULL)
       {
        switch(i)
        {
            case 0: x=SCREEN_W/30; y=SCREEN_H/40; break;
            case 1: x=SCREEN_W-SCREEN_W/30-SCREEN_W/4; y=SCREEN_H/40; break;
            case 2: x=SCREEN_W/30; y=3*SCREEN_H/40; break;
            case 3: x=SCREEN_W-SCREEN_W/30-SCREEN_W/4; y=3*SCREEN_H/40;break;
            case 4: x=SCREEN_W/30; y=5*SCREEN_H/40;break;
        }
        if(perso[i]->couleur!=makecol(100,100,100))
        buffer=barre(buffer,x,y,perso[i]->couleur, perso,i,ed);
        textprintf_ex(buffer, font,x +SCREEN_W/8-SCREEN_W/30,y+SCREEN_H/80, makecol(0, 0, 0),
		    -1, "%d / %d",(int)perso[i]->life, (int)ed->life_max);
       }
   }
    return buffer;
}


BITMAP* barre(BITMAP* buffer,int x,int y, int couleur, t_perso** perso, int i, t_editeur* ed)
{

rectfill(buffer,x,y,x+((float)perso[i]->life/(float)ed->life_max)*(float)(SCREEN_W/4),y+SCREEN_H/30, couleur);
rect(buffer,x,y,x+SCREEN_W/4,y+SCREEN_H/30,makecol(0,0,0));
    return buffer;
}

BITMAP* cursor(BITMAP* buffer,BITMAP* sprite,t_perso** perso, t_editeur* ed)
{
    int i;
    float width=SCREEN_H/60;
    float length=width;
    float radius=sprite->w/5;
    int g;
    int x,y;
    float rat=1;
    float rad=0.0174532925;
    int dehors=0;
    BITMAP* cercle=create_bitmap(3*radius,3*radius);


    for(i=0;i<ed->nb_persos_max;i++)
    {
        dehors=0;
        if(perso[i]!=NULL)
        {
            radius=sprite->w/5;
            x=(int)perso[i]->x;
y=(int)perso[i]->y;

if(perso[i]->x+sprite->w/2<0){dehors=1;}
if(perso[i]->x>SCREEN_W){dehors=2;}
if(dehors==1){x=0; }
if(dehors==2)x=SCREEN_W-3*radius;


            clear_to_color(cercle,makecol(255,0,255));
//            rectfill(buffer,perso[i]->x+sprite->w/2-length/2, perso[i]->y-1.5*width,perso[i]->x+sprite->w/2+length/2, perso[i]->y-0.5*width,  perso[i]->couleur);

if(dehors!=0)circlefill(cercle,cercle->w/2,cercle->h/2,1.5*radius,makecol(255,0,0));

       circlefill(cercle,cercle->w/2,cercle->h/2,radius,perso[i]->couleur);
       rat=perso[i]->life/ed->life_max;
       rat=360*rat;
       rat=360-rat;
       for(g=0;g<rat;g++)
       {
           line_epaisse(cercle,cercle->w/2,cercle->h/2,cercle->w/2 +radius*cos(g*rad),cercle->h/2+ radius*sin(-g*rad),makecol(255,0,255));
       }



        draw_sprite(buffer,cercle,x+sprite->w/2-cercle->w/2,y-2.5*radius);

        }

    }

destroy_bitmap(cercle);
   return buffer;
}


void scrolling(t_editeur* ed,t_perso** perso,btmp image)
{
  int i;
  float x_max=0;
  int p=0;
  for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
       if(perso[i]->x>x_max){x_max=perso[i]->x;p=i;}
      }
  }
   if(x_max>SCREEN_W/2)
   { ed->scroll_x_old=ed->scroll_x;
if(perso[p]!=NULL)
{
   while(perso[p]->x+ed->scroll_x_old-ed->scroll_x>SCREEN_W/2)
   {
     ed->scroll_x++;
   }
}
//       ed->scroll_x=x_max+image.sprite_perso[i]->w/2-SCREEN_W/2;
 for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
     perso[i]->x+=ed->scroll_x_old-ed->scroll_x;
      }
  }


   }


    if(x_max<SCREEN_W/6)
   { ed->scroll_x_old=ed->scroll_x;
if(perso[p]!=NULL)
{
 while(perso[p]->x+ed->scroll_x_old-ed->scroll_x<SCREEN_W/6)
   {
     ed->scroll_x--;
   }
}


//       ed->scroll_x=x_max+image.sprite_perso[i]->w/2-SCREEN_W/2;
 for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
     perso[i]->x+=ed->scroll_x_old-ed->scroll_x;
      }
  }

   }

}


BITMAP* background_scroll(btmp image, t_editeur* ed)
{
   int g=ed->scroll_x/image.background[0]->w;
int x=g*image.background[0]->w-ed->scroll_x;

draw_sprite(image.buffer,image.background[0],x,0);
draw_sprite(image.buffer,image.background[0],x+image.background[0]->w,0);

    return image.buffer;
}


BITMAP* pieces_afficher(BITMAP* piecee, BITMAP* buffer,t_perso** perso,t_editeur* ed,t_objet** piece)
{
    int i;
    int pieces_tot=0;

    for(i=0;i<ed->nb_persos_max;i++)
    {
        if(perso[i]!=NULL)
        {
          pieces_tot+=perso[i]->pieces;
        }
    }

    for(i=0;i<ed->nb_pieces_max;i++)
    {
        if(piece[i]!=NULL)
        {
//            draw_sprite(screen,piecee,piece[i]->x-ed->scroll_x,piece[i]->y);
           masked_blit(piecee,buffer,piece[i]->sprite_etat*((float)piecee->w/20),0,piece[i]->x-ed->scroll_x,piece[i]->y,(float)piecee->w/20,piecee->h );
        }
    }



    return buffer;
}


void sprite_next_piece(t_objet** piece, t_editeur* ed)
{
   int i;
   for(i=0;i<ed->nb_pieces_max;i++)
   {
       if(piece[i]!=NULL)
       {
           piece[i]->compteur-=ed->fps_ratio;
           if(piece[i]->compteur<=0)
           {
               piece[i]->compteur=ed->compteur_max;
               piece[i]->sprite_etat++;
               if(piece[i]->sprite_etat>19)
               {
                   piece[i]->sprite_etat=0;
               }
           }



       }

   }



}


BITMAP* beta_coord(t_perso** perso,t_editeur* ed, BITMAP* buffer)
{
    int i;
if(perso[ed->perso_select]!=NULL)
  textprintf_ex(buffer, font,SCREEN_W/6,SCREEN_H/6, makecol(0, 0, 0),
		    -1, "%d    %d   %d",(int)ed->fps, (int)perso[ed->perso_select]->vit_y, (int)perso[ed->perso_select]->cooldown);

    return buffer;
}


BITMAP* afficher_nb_pieces(BITMAP* buffer, BITMAP* image, int pieces)
{
    masked_blit(image,buffer,0,0,SCREEN_W-4*image->w/20,2*image->h,image->w/20,image->h);
//    draw_sprite(buffer,image,);
     textprintf_ex(buffer, font, SCREEN_W-3*image->w/20,3*image->h, makecol(0, 0, 0),
		    -1, "%d", pieces);

    return buffer;
}


BITMAP* moving_clouds(t_objet** nuage, BITMAP* buffer,BITMAP* cloud[2],float scroll_x, int nb, float r)
{
    int i;
    int g=0;

    for(i=0;i<nb;i++)
    {
        if(nuage[i]!=NULL)
        {
            if(nuage[i]->interactif==0)
            {
          nuage[i]->x=-scroll_x+nuage[i]->compteur ;

nuage[i]->compteur-=r*nuage[i]->vit_x/100;
nuage[i]->cs-=r*nuage[i]->vit_y*0.0001;
if(nuage[i]->cs<=0)nuage[i]->cs=360;
        if(nuage[i]->compteur+cloud[g]->w<=scroll_x)nuage[i]->compteur=SCREEN_W+cloud[g]->w+scroll_x;

        nuage[i]->y=3*cloud[g]->h+cloud[g]->h*cos(nuage[i]->cs);
        }

        if(nuage[i]->interactif==1)
        {
       if(pow(nuage[i]->cote,2)==4)
       {
nuage[i]->x=-scroll_x+nuage[i]->compteur ;

       }

       else{
//            g=0;

        nuage[i]->x=-scroll_x+nuage[i]->compteur ;

nuage[i]->compteur+=r*nuage[i]->cote*nuage[i]->vit_x/100;
nuage[i]->cs-=r*nuage[i]->vit_y*0.0001;
if(nuage[i]->cs<=0)nuage[i]->cs=360;
        if(nuage[i]->compteur+cloud[g]->w<=scroll_x)nuage[i]->compteur=SCREEN_W+cloud[g]->w+scroll_x;

        nuage[i]->y+=nuage[i]->y_cot*nuage[i]->vit_y/100;
       }


        }
        }

    }


buffer=affiche_boo(nuage,nb,cloud, buffer);



   return buffer;
}


BITMAP* affiche_boo(t_objet** nuage,int nb, BITMAP* cloud[5], BITMAP* buffer)
{
    int i;
    int g=0;

     for(i=0;i<nb;i++)
    {
        if(nuage[i]!=NULL)
        {
if(nuage[i]->cote==1)g=1;
if(nuage[i]->cote==-1)g=0;

if(nuage[i]->cote==2)g=3;
if(nuage[i]->cote==-2)g=2;

    draw_sprite(buffer,cloud[g],nuage[i]->x,nuage[i]->y);
    printf("%d\n", g);
        }
    }


    return buffer;
}



btmp affiche_pipes(t_pipe** pipe,t_editeur* ed,btmp image, t_perso** perso)
{

  int i;
  for(i=0;i<ed->nb_pipes_max;i++)
  {
      if(pipe[i]!=NULL)
      {
          if((pipe[i]->x_1-ed->scroll_x>0)&&(pipe[i]->x_1-ed->scroll_x<7*SCREEN_W/6))
          {

         image=charge_obstacle(image,image.buffer_obst,image.pipe,pipe[i]->x_1-ed->scroll_x,pipe[i]->y_1,makecol(255,0,0), 1);
          }

           if(pipe[i]->x_2-ed->scroll_x>0&&pipe[i]->x_2-ed->scroll_x<7*SCREEN_W/6)
          {
        image=charge_obstacle(image,image.buffer_obst,image.pipe,pipe[i]->x_2-ed->scroll_x,pipe[i]->y_2,makecol(255,0,0), 1);
          }
      }
  }

  return image;
}





void camera_clic_move(t_editeur* ed, BITMAP* mape)
{
  show_mouse(screen);

  if(mouse_b&1)
  {
      ed->scroll_x=((float)mouse_x/(float)SCREEN_W)*(mape->w-SCREEN_W);
      ed->show_map=1;
//     do{rest(1);} while(mouse_b&1);
  }

}



void show_map_advancement(BITMAP* buffer,float scroll, int width)
{
    float g=7;
    float W=(float)SCREEN_W;
     float H=(float)SCREEN_H;
     float ep=20;



     float wdth=W/g;

      float w_vue=W/width;
     float hgt=H/g;
     float sc_f=(float)(scroll/(float)width);

    rectfill(buffer, 0,0,wdth,hgt, makecol(255,0,0)  );
    rectfill(buffer, 0+wdth/ep,0+hgt/ep,wdth-wdth/ep,hgt-hgt/ep, makecol(255,0,255)  );

    rectfill(buffer, 0+wdth/ep+sc_f*wdth,0+hgt/ep,sc_f*wdth+w_vue,hgt-hgt/ep, makecol(0,0,255)  );


}



void chgmt_map(btmp image,t_perso** perso,t_editeur* ed,t_pipe** pipe,t_objet** bloc,t_objet** nuage,t_objet** boo,t_objet** piece)
{
    int i;
    for(i=0;i<ed->nb_persos_max;i++)
    {
        free(perso[i]);
    }

  for(i=0;i<ed->nb_pipes_max;i++)
  {
      free(pipe[i]);
  }

    for(i=0;i<ed->nb_blocks_max;i++)
    {
        free(bloc[i]);
    }

    for(i=0;i<ed->weather_max;i++)
    {
        free(nuage[i]);
    }

    for(i=0;i<ed->nb_pieces_max;i++)
    {
        free(piece[i]);
    }

    free(ed);

}
