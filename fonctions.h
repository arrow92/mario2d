#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED


#include <allegro.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



#define NBMAX_PRIMIVE 100


typedef struct prim
{

}t_primitive;


typedef struct personnage
{
    float x;
    float y;
    float acc_x;
    float acc_y;
    float vit_x;
    float vit_y;
    float poids;
    int direction;
    int idle;
    int land;
    float life;
    float life_max;
    float life_old;
    int pieces;
    float cooldown;
    float cooldown_sprite;
    float cooldown_sol;
    float cooldown_life;
    float cooldown_move;
    int action;
    float taille;
    int contact;
    int deg[361];
    int butt;
    int couleur;
    int sprite_etat;
    float compteur_idle;
    int etat;
    int character;


}t_perso;



typedef struct image
{
BITMAP* buffer;
BITMAP* buffer_persos;

BITMAP* sprite_perso[5];
BITMAP* perso[8];
BITMAP* buffer_obst;
BITMAP* cliff_long;
BITMAP* rotation;
BITMAP* background[5];
BITMAP* bckg;
BITMAP* buffer_bckg;
BITMAP* buffer_enemi;
BITMAP* block;
BITMAP* b_block;
BITMAP* piece;
BITMAP* piece_b;
BITMAP* piece_collision;
BITMAP* cloud[5];
BITMAP* boo[5];
BITMAP* pipe;
BITMAP* pipe_2;
BITMAP* pipe_3;
BITMAP* buffer_map;
BITMAP* title;
BITMAP* title_1;
BITMAP* voile;
BITMAP* para;
BITMAP* goom;
BITMAP* buffer_obj;
BITMAP* fireball;
BITMAP* flag;
BITMAP* antig[2];
}btmp;

typedef struct fichiers
{
FILE* save;
FILE* falaise;
FILE* falaise_write;
FILE* bloc;
FILE* b_bloc;
FILE* para;
FILE* goom;
FILE* pieces;
FILE* pipes;
FILE* antig;
}t_fichier;


typedef struct EFFECT
{
    SAMPLE* title;
    SAMPLE* level;
    SAMPLE* coin;
    SAMPLE* level_end;
    SAMPLE* k_shell;
    SAMPLE* hurt[5];
    SAMPLE* warp;
    SAMPLE* fireball;
}t_effect;


typedef struct editeur
{
int nb_persos_max;
int perso_select;
float acc_x;
float acc_y;
int sprite_changed;
int life_max;
float vit_x_max;
float vit_y_max;
float scroll_x;
float scroll_x_old;
int nb_blocks_max;
int nb_b_blocks_max;
int nb_goom_max;
int nb_pieces_max;
float compteur_max;
int pieces;
int pieces_old;
int weather;
int weather_max;
int nb_boo;
int nb_boo_max;
int nb_pipes_max;
float fps_max;
float fps;
float fps_ori;
float fps_ratio;
float key_cooldown;
int show_map;
char tab[5][6];
int cursor;
int c_map;
t_perso** backup;
int nb_para_max;
float obj_falling_speed;
int nb_fire_max;
int nb_antig_max;
}t_editeur;


typedef struct cliffs
{
    int x[100];
    int y[100];
    float rot[100];
    int nombre;
}t_cliff;

typedef struct obj
{
    float x;
    float y;
    float x_2;
    float y_2;
    int bonus;
    int sprite_etat;
    float compteur;
    float cs;
    float vit_y;
    float vit_x;
    int interactif;
    int cote;
    int y_cot;
    int type;
    float y_ori;
    float x_ori;
    float cx;
    float cooldown;
    int cast;

}t_objet;



typedef struct warp
{
    float x_1;
    float y_1;
    float x_2;
    float y_2;
}t_pipe;


#include "fonctionsalleg.h"

void init_editeur(t_editeur* ed);
void create_perso(t_perso** perso, t_editeur* ed,int i);
void sprite_change(t_perso** perso,int i, char k_change);
void tout_afficher_screen(btmp image,t_editeur* ed,t_perso** perso,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** piece,t_objet** nuage,t_objet** boo,t_objet** pipe,t_objet** fireball,t_objet** antig);
BITMAP* blit_perso(BITMAP* sprite, BITMAP* source, int dec_x,int dec_y,int direction);
btmp perso_idle(btmp image, t_perso** perso,int i);
void gravite(t_perso** perso,int i,t_editeur* ed,int contact);
void contact(t_perso** perso, t_editeur* ed, btmp image, int i, int j);
void detect_sol(t_perso** perso, btmp image,t_editeur* ed);
btmp charge_obstacle(btmp image,BITMAP* bmp, BITMAP* pic, float x, float y, int couleur, int draw);
btmp affiche_persos(btmp image, t_perso** perso, t_editeur* ed);
int contact_sol(t_perso** perso,btmp image,int i,t_editeur* ed);
btmp charge_obstacle_turned(btmp image,BITMAP* bmp, BITMAP* pic,BITMAP* temp, float x, float y,int h,int w,float rotation);
FILE* charger_fichier(FILE* file, char* chaine);
BITMAP* barre(BITMAP* buffer,int x,int y, int couleur, t_perso** perso, int i, t_editeur* ed);
BITMAP* barre_de_vie(t_perso** perso,t_editeur* ed, BITMAP* buffer);
void punch(t_perso** perso,t_editeur* ed, btmp image);
int distance_contact(btmp image,BITMAP* obj_1,BITMAP* obj_2,float x_1,float y_1,float x_2,float y_2, float rayon);
void detect_dead(t_perso** perso, t_editeur* ed,btmp image);
void collision_persos(t_perso** perso, t_editeur* ed, btmp image);
void collision_cote(t_perso** perso, t_editeur* ed,btmp image,int i);
btmp border_draw(t_perso** perso, btmp image,t_editeur* ed);
void scrolling(t_editeur* ed,t_perso** perso,btmp image);
BITMAP* background_scroll(btmp image, t_editeur* ed);
btmp charge_obstacle_intel(btmp image,BITMAP* bmp, BITMAP* pic,t_perso** perso, t_editeur* ed, float x, float y, int couleur, int draw);

void create_piece(t_objet** piece,t_editeur* ed, btmp image);
BITMAP* pieces_afficher(BITMAP* piecee, BITMAP* buffer,t_perso** perso,t_editeur* ed,t_objet** piece);
void detect_piece(t_perso** perso, t_objet** piece,t_editeur* ed,btmp image,int nb_max,t_effect* effect);
void detect_bloc(t_perso** perso, t_objet** bloc,t_editeur* ed,btmp image,t_effect* effect,int nb_max,int W,int H);
void charger_blocs(t_objet** bloc,t_editeur* ed, t_fichier fichier,int nb_max, char* path);
BITMAP* beta_coord(t_perso** perso,t_editeur* ed, BITMAP* buffer);
void bonus(t_perso** perso, t_editeur* ed,int i,int bonus);
BITMAP* afficher_nb_pieces(BITMAP* buffer, BITMAP* image, int pieces);
void ini_nuages(t_objet**  nuage,t_editeur* ed);
BITMAP* moving_clouds(t_objet** nuage, BITMAP* buffer,BITMAP* cloud[2],float scroll_x, int nb, float r);
void obj_inter_cote(t_perso** perso, t_objet** nuage, int nb, int nb_persos);
int creer_boo(t_objet** objet, int nb,int interactif);
BITMAP* affiche_boo(t_objet** boo,int nb, BITMAP* cloud[5], BITMAP* buffer);
void creer_pipe(t_objet** pipe, t_editeur* ed, float x_1,float y_1, float x_2,float y_2);
btmp affiche_pipes(t_objet** pipe,t_editeur* ed,btmp image,t_perso** perso);
void take_warp(t_perso** perso,t_editeur* ed, t_objet** pipe,t_effect* effect,btmp image);
void editeur_fps_adapt(t_editeur* ed);
void cooldown_reduce(t_perso** perso, t_editeur* ed, btmp image);
void set_cooldown(t_perso** perso,int i, float time);
void set_cooldown_life(t_perso** perso,int i, float time);
void set_cooldown_move(t_perso** perso,int i, float time);
void camera_clic_move(t_editeur* ed, BITMAP* mape);
void show_map_advancement(BITMAP* buffer,float scroll, int width);
void set_cooldown_sprite(t_perso** perso,int i, float time);
void beta_fly(t_perso**perso,t_editeur* ed,int dir);
void acceleration(t_perso** perso, t_editeur* ed,int i);
int* angle_moyen(t_perso** perso, float g, btmp image,int tab[3], int i,t_editeur* ed);
void JUMP(t_perso** perso, t_editeur* ed, char k_jump,int sel);
void JUMP_beta(t_perso** perso, t_editeur* ed, char k_jump,int sel);
void GAUCHE_DROITE(t_perso** perso,t_editeur* ed,btmp image,char left,char right, int sel);

void WARP(t_perso** perso, t_editeur* ed,btmp image, t_objet** pipe,t_effect* effect, char down, int sel);
void PUNCH(t_perso** perso,t_objet** bloc,t_effect* effect, t_editeur* ed,btmp image, char p,int sel);
BITMAP* cursor(BITMAP* buffer,BITMAP* sprite,t_perso** perso, t_editeur* ed);
int arrivee(t_perso** perso, t_editeur* ed, btmp image, int x_arrivee);
int check_arrivee(t_perso** perso,t_editeur* ed, btmp image, int check);
t_editeur* ALLOCATION_MONDE(btmp image,t_perso** perso,t_objet** pipe,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** nuage,t_objet** boo,t_objet** piece,t_objet** fireball,t_objet** antig,t_fichier fichier,t_effect* effect,int c_map);
t_editeur* LOAD_NEXT_MAP(btmp image,t_perso** perso,t_objet** pipe,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** nuage,t_objet** boo,t_objet** piece,t_objet** fireball,t_objet** antig,t_fichier fichier,t_effect* effect, int c_map,t_editeur* ed);
BITMAP* animation_perso(BITMAP* sprite, BITMAP* source, t_perso** perso,t_editeur* ed, int i);
void set_cooldown_sol(t_perso** perso,int i, float time);
void sprite_next_piece(t_objet** piece, t_editeur* ed,int nb_max,int compteur_max, int s_max);
void carreaux(int x,int y,int W, int H);
void creer_bloc(t_objet** bloc,t_editeur* ed,int nb_max, int x,int y);
btmp block_static(btmp image, t_editeur* ed, t_objet** bloc,BITMAP* block,int nb_max,int c_map);
btmp block(btmp image, t_editeur* ed, t_objet** bloc,BITMAP* block,int W,int H,int nb_max,int cote);
void life_check(t_perso** perso,t_editeur* ed,t_effect* effect);
int nb_elements_fichier(char* path, int mots_ligne);
void menu(btmp image,t_perso** perso,t_objet** pipe,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** nuage,t_objet** boo,t_objet** piece,t_fichier fichier,t_editeur* ed,t_effect* effect);
void cave(t_perso** perso,t_editeur* ed,BITMAP* buffer,BITMAP* voile,BITMAP* p,float radius);
void gravite_objet(t_objet** obj,float fps_ratio,int nb_max);
void objet_passif(t_objet** bloc,t_perso** perso,btmp image,t_editeur* ed,int i,int j,BITMAP* bk);
void objet_hostile(t_objet** bloc,t_perso** perso,btmp image,t_editeur* ed,int i,int j,t_effect* effect,BITMAP* bk);
void deplacement_para(t_objet** bloc,t_editeur* ed,BITMAP* sprite,int nb_max);
void life_change(t_perso** perso,t_editeur* ed,int hp,int i);
void deplacement_goom(t_objet** bloc,t_editeur* ed,BITMAP* sprite,BITMAP* buffer_obst,int ra,int nb_max);
void obj_sol(t_objet** bloc,BITMAP* sprite,BITMAP* buffer_obst,int ra,int i);
void coin_dep(t_objet** bloc,t_editeur* ed,BITMAP* sprite,int nb_max);
void deplacement_para_solo(t_objet** bloc,t_editeur* ed,BITMAP* sprite,int i,int nb_max);
void set_cooldown_move_all(t_perso** perso,int nb_max, float time);
void take_pipe_a(t_objet** bloc, t_editeur* ed,btmp image, t_perso** perso,int in,int x,int y,int i);
void etat_mario(t_perso** perso,int i, int etat);
void throw_ball(BITMAP* sprite,t_effect* effect,t_perso** perso,t_objet** bloc, t_editeur* ed, int i,int nb_max);
void set_cooldown_obj(t_objet** bloc,int i, float time);
void cooldown_reduce_bloc(t_objet** bloc, t_editeur* ed,int nb_max);
void rebond_fire(t_objet** bloc,t_editeur* ed,BITMAP* obst,BITMAP* sprite,int ra,int nb_max);
void obj_obj_hostile(btmp image,t_perso** perso,t_objet** fire,t_objet** bloc,BITMAP* block,BITMAP* sprite,t_editeur* ed,t_effect* effect,int ra_b,int ra_s,int nb_max);
void sauter_beta(t_perso** perso,t_editeur* ed,int direction);
BITMAP* display_finish(BITMAP* finish,BITMAP* sprite,BITMAP* buffer, t_objet** bloc,int nb_max, int x_arrivee);
btmp antigravite(btmp image, t_editeur* ed, t_objet** bloc,BITMAP* block[2],int W,int H,int nb_max,int cote);

#endif // FONCTIONS_H_INCLUDED
