#include "fonctions.h"


t_fichier maps(t_fichier fichier,int Mapx,int Mapy,btmp image,t_cliff cliff)
{

char n_falaise[50];

sprintf(n_falaise,"levels/falaise_level_%d_%d.txt", Mapx,Mapy);

fichier.falaise=NULL;


fichier.falaise=charger_fichier(fichier.falaise, n_falaise);


fscanf(fichier.falaise,"%d", &cliff.nombre);

if(cliff.nombre!=0)
{
  int falaisetraite;
for(falaisetraite=0;falaisetraite<cliff.nombre;falaisetraite++)
{
    fscanf(fichier.falaise,"%d %d %f",&cliff.x[falaisetraite],&cliff.y[falaisetraite],&cliff.rot[falaisetraite]);
    clear_to_color(image.rotation,makecol(255,0,255));

image=charge_obstacle_turned(image,image.buffer,image.cliff_long,image.rotation,cliff.x[falaisetraite],cliff.y[falaisetraite],SCREEN_W,SCREEN_H,cliff.rot[falaisetraite]);

}

}


rest(100);

fclose(fichier.falaise);

return fichier;
}


FILE* charger_fichier(FILE* file, char* chaine)
{
    int error;
    do{
file=fopen(chaine,"r");
error=0;
if(file==NULL)
{
    fclose(file);
    file=fopen(chaine,"w");
    fprintf(file,"0");
    fclose(file);
    error=1;
}
}while(error==1);

return file;
}

void charger_blocs(t_objet** bloc,t_editeur* ed, t_fichier fichier)
{
    int nb=0;
    int i;
fichier.bloc=fopen("data/bloc_test.txt", "r");
fscanf(fichier.bloc,"%d", &nb);

for(i=0;i<nb;i++)
{
    if(i<ed->nb_blocks_max)
    {
        bloc[i]=malloc(sizeof(t_objet));
        bloc[i]->sprite_etat=0;
    }
}

for(i=0;i<nb;i++)
{
    if(i<ed->nb_blocks_max)
    {
        fscanf(fichier.bloc, "%f %f %d", &bloc[i]->x,&bloc[i]->y, &bloc[i]->bonus);
    }
}

fclose(fichier.bloc);
}


void ini_nuages(t_objet**  nuage, t_editeur* ed)
{
   int i;


}


void creer_pipe(t_pipe** pipe, t_editeur* ed, float x_1,float y_1, float x_2,float y_2)
{
   int i;
   for(i=0;i<ed->nb_pipes_max;i++)
   {
       if(pipe[i]!=NULL)
       {

       }
       else
        {
        pipe[i]=malloc(sizeof(t_pipe));
       pipe[i]->x_1=x_1;
       pipe[i]->x_2=x_2;
       pipe[i]->y_1=y_1;
       pipe[i]->y_2=y_2;
break;
       }
   }



}


BITMAP* LOAD_NEXT_MAP(btmp image,t_perso** perso,t_pipe** pipe,t_objet** bloc,t_objet** nuage,t_objet** boo,t_objet** piece,t_fichier fichier, int c_map,t_editeur* ed)
{
       chgmt_map(image,perso,ed,pipe,bloc,nuage,boo,piece);
    if(c_map<10)c_map++;
                  char bckg[50];
sprintf(bckg,"textures/background_%d.bmp", c_map);
image.bckg=load_bitmap(bckg, NULL);

    ed=ALLOCATION_MONDE(image,perso,pipe,bloc,nuage,boo,piece,fichier,c_map);

  return image.bckg;
}
