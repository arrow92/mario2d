#ifndef FONCTIONSALLEG_H_INCLUDED
#define FONCTIONSALLEG_H_INCLUDED




void lancerAllegro(int largeur, int hauteur);
BITMAP *charger_image(char *source);
BITMAP *creer_image(int x,int y);
BITMAP* fade_image(BITMAP* buffer,BITMAP* bmp,int x,int y,int couleur_depart,int couleur_arrive,int vitesse);
void line_epaisse(BITMAP *bmp, int x1, int y1, int x2, int y2, int coul);

#endif // FONCTIONSALLEG_H_INCLUDED
