
#include "fonctions.h"

void lancerAllegro(int largeur, int hauteur)
{
    allegro_init();
    install_keyboard();
    install_sound(DIGI_AUTODETECT,MIDI_AUTODETECT,"A");
    if(install_mouse() == -1)
    {
        allegro_message("Erreur (souris): %s",allegro_error);
        exit(EXIT_FAILURE);
    }
    set_color_depth(desktop_color_depth());
    if(set_gfx_mode(GFX_AUTODETECT_WINDOWED,largeur,hauteur,0,0) != 0)
    {
        set_gfx_mode(GFX_TEXT,0,0,0,0);
        allegro_message("Erreur (mode vid�o) : %s",allegro_error);
        allegro_exit();
        exit(EXIT_FAILURE);
    }

install_joystick(JOY_TYPE_AUTODETECT);


}





BITMAP *charger_image(char *source)
{
    BITMAP *image;
    image = load_bitmap(source,NULL);
    if(!image)
    {
        set_gfx_mode(GFX_TEXT,0,0,0,0);
        allegro_message("Erreur ! Impossible de lire le fichier image %s !",source);
        exit(EXIT_FAILURE);
    }
    return image;
}

BITMAP *creer_image(int x,int y)
{
    BITMAP *image;
    image = create_bitmap(x,y);
    if(!image)
    {
        set_gfx_mode(GFX_TEXT,0,0,0,0);
        allegro_message("Erreur ! Impossible de creer le bitmap");
        exit(EXIT_FAILURE);
    }
    return image;
}


BITMAP* fade_image(BITMAP* buffer,BITMAP* bmp,int x,int y,int couleur_depart,int couleur_arrive,int vitesse)
{

  clear_to_color(buffer,makecol(255,0,255));
  int i,j;
  int r,v,b;
  int c;
 int color;
 if(couleur_depart>couleur_arrive)
 {
  for(color=couleur_depart;color>couleur_arrive;color-=vitesse)
  {
  for(i=0;i<bmp->w;i++)
  {
      for(j=0;j<bmp->h;j++)
      {
          if((key[KEY_B])||(key[KEY_A])) return buffer;
        c=getpixel(bmp,i,j);
        if(c!=makecol(255,0,255))
        {
            r=getr(c);
            v=getg(c);
            b=getb(c);
            r+=color; if(r>couleur_depart)r=couleur_depart;
             v+=color; if(v>couleur_depart)v=couleur_depart;
              b+=color; if(b>couleur_depart)b=couleur_depart;
              putpixel(buffer,i,j,makecol(r,v,b));

        }



      }
  }
  draw_sprite(screen,buffer,x,y);
  }
 }

 else if(couleur_depart<couleur_arrive)
 {
  for(color=couleur_depart;color<couleur_arrive;color+=vitesse)
  {

  for(i=0;i<bmp->w;i++)
  {
      for(j=0;j<bmp->h;j++)
      {
          if((key[KEY_B])||(key[KEY_A])) return buffer;
        c=getpixel(bmp,i,j);
        if(c!=makecol(255,0,255))
        {

            r=getr(c);
            v=getg(c);
            b=getb(c);
          r-=color; if(r<couleur_depart)r=couleur_depart;
             v-=color; if(v<couleur_depart)v=couleur_depart;
              b-=color; if(b<couleur_depart)b=couleur_depart;
//              printf("R %d---V %d---B %d\n", r,v,b);
//              rest(100);
              putpixel(buffer,i,j,makecol(r,v,b));

        }



      }
  }
  draw_sprite(screen,buffer,x,y);
  }
 }
//destroy_bitmap(buffer);
return buffer;
}


void line_epaisse(BITMAP *bmp, int x1, int y1, int x2, int y2, int coul)
{
    line(bmp, x1, y1, x2, y2, coul);
    line(bmp, x1+1, y1, x2+1, y2, coul);
    line(bmp, x1-1, y1, x2-1, y2, coul);
    line(bmp, x1, y1+1, x2, y2+1, coul);
    line(bmp, x1, y1-1, x2, y2-1, coul);
}
