#include "fonctions.h"




btmp charge_obstacle(btmp image,BITMAP* bmp, BITMAP* pic, float x, float y, int couleur, int draw)
{
 int i,j;
 int c;

 if(draw==1)
    draw_sprite(image.buffer,pic,x,y);
 for(i=0;i<pic->w;i++)
 {
     for(j=0;j<pic->h;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }




 return image;
}


btmp charge_obstacle_intel(btmp image,BITMAP* bmp, BITMAP* pic,t_perso** perso, t_editeur* ed, float x, float y, int couleur, int draw)
{
 int i,j;
 int c;
 int g;

 if(draw==1)
    draw_sprite(image.buffer,pic,x,y);

    for(g=0;g<ed->nb_persos_max;g++)
    {
        if(perso[g]!=NULL)
        {
//               image.sprite_perso[i]->h/5;j<4*image.sprite_perso[i]->h/5
         for(i=perso[g]->x;i<perso[g]->x+3*image.sprite_perso[g]->w/5;i++)
 {
     for(j=perso[g]->y-image.sprite_perso[g]->h/5;j<perso[g]->y+image.sprite_perso[g]->h;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur); //gauche
        }
     }

 }

    for(i=perso[g]->x+3*image.sprite_perso[g]->w/5;i<perso[g]->x+image.sprite_perso[g]->w;i++)
 {
     for(j=perso[g]->y-image.sprite_perso[g]->h/5;j<perso[g]->y+image.sprite_perso[g]->h;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }


     for(i=perso[g]->x;i<perso[g]->x+image.sprite_perso[g]->w;i++)
 {

     for(j=perso[g]->y-image.sprite_perso[g]->h/6;j<perso[g]->y+image.sprite_perso[g]->h/6;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }

   for(i=perso[g]->x;i<perso[g]->x+image.sprite_perso[g]->w;i++)
 {
     for(j=perso[g]->y+4*image.sprite_perso[g]->h/6;j<perso[g]->y+9*image.sprite_perso[g]->h/6;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }


        }
    }





 return image;
}



int contact_sol_beta(t_perso** perso,btmp image,int i, t_editeur* ed)
{
float r=ed->fps_ratio;
   int contact=0;
   int j,g;
   int c;
   float poids=(float)SCREEN_H/(5000*20);
if(perso[i]!=NULL)
{
    for(g=0;g<3*r;g++)
       {
   for(j=image.perso->w/24;j<image.perso->w/12;j+=10)
   {

//       putpixel(screen,perso[i]->x+j,perso[i]->y-image.perso->h/60+image.perso->h/4-g,makecol(255,0,0));
        c=getpixel(image.buffer_obst,perso[i]->x+j,perso[i]->y-image.perso->h/60+image.perso->h/4-g);
       if(c==makecol(255,0,0))

       {

if(g==0){
           perso[i]->vit_y=0;
           perso[i]->acc_y=0;
           contact=1;
}

else{
   perso[i]->y-=g/2;

}

       }



          if(j>image.perso->w/24 &&j<image.perso->w/12-10)
        {
//             putpixel(screen,perso[i]->x+j,perso[i]->y+image.perso->h/60-g,makecol(0,255,0));
       if(getpixel(image.buffer_obst,perso[i]->x+j,perso[i]->y+image.perso->h/60-g)==makecol(255,0,0))
       {

            perso[i]->vit_y-=g*pow(pow(perso[i]->vit_y,2),1/2)/4;
           perso[i]->acc_y=0;
       }
    }

   }
   }
}


    return contact;
}



int* angle_moyen(t_perso** perso, float g, btmp image,int tab[3], int i, t_editeur* ed)
{
int c,d; float r=ed->fps_ratio;
float rad=0.0174532925;
int signe=1;
if(ed->acc_y<0)signe=-1;
int temp=1;

  for(g=0;g<360;g++)
{


perso[i]->deg[(int)g]=0;



//if((int)g%10==0)putpixel(screen,perso[i]->x+image.sprite_perso[i]->w/2 +cos(g*rad)*2*perso[i]->taille/3+r*perso[i]->vit_x, perso[i]->y+image.sprite_perso[i]->h/2+sin(-g*rad)*perso[i]->taille +r*perso[i]->vit_y,makecol(255,0,0) );
 c=getpixel(image.buffer_obst,perso[i]->x+image.sprite_perso[i]->w/2 +cos(g*rad)*2*perso[i]->taille/3+r*perso[i]->vit_x, perso[i]->y+image.sprite_perso[i]->h/2+sin(-g*rad)*perso[i]->taille*signe +r*perso[i]->vit_y );
 if(c==(makecol(255,0,0)))
 {

     perso[i]->deg[(int)g]=1;
     tab[0]++;
     tab[1]+=g;

 }

 if(c==makecol(0,255,0))
 {
//     perso[i]->vit_x=-0.1*perso[ed->perso_select]->direction*ed->vit_x_max;
    perso[i]->life-=0.005*r;
 }

 if(c==makecol(0,100,0))
 {
     if(ed->acc_y>0)
     {
         ed->acc_y=-ed->acc_y;

     }

 }

  if(c==makecol(0,110,0))
 {
     if(ed->acc_y<0)
     {


     ed->acc_y=-ed->acc_y;

     }
 }

}
  return tab[3];
}


int contact_sol(t_perso** perso,btmp image,int i, t_editeur* ed)
{
float r=ed->fps_ratio;
   int contact=0;
   float j,g;
   int c;
   int compteur;
   int sortir=0;
   float poids=9.81/20;
   float rad=0.0174532925;
   int d=0;
   int rate=0;
   int tab[3]={0};
   int mur=0;
   int murr=0;
   int h;
   int signe=1;
   if(ed->acc_y<0)signe=-1;


if(perso[i]!=NULL)
{

compteur=0;
int temp=1;

while(compteur<20)
{
      mur=0;
      murr=0;
    temp=1;
tab[3]=angle_moyen(perso, g,image,tab, i,ed);
d=tab[0];
rate=tab[1];
g=(float)rate/(float)d;


printf("%d\n", (int)g);

if(d>0)
{
if((g>270-40)&&(g<270+40))
{
perso[i]->vit_y=0;
//    perso[i]->vit_y-=ed->vit_y_max;
     if(perso[i]->vit_y>0)perso[i]->vit_y=0;

if(signe==1)
{
   for(h=3*image.sprite_perso[i]->h/4;h<image.sprite_perso[i]->h-4;h++)
   {
       if(getpixel(image.buffer_obst,perso[i]->x+image.sprite_perso[i]->w/2 , perso[i]->y+h )==makecol(255,0,0))
       {
           perso[i]->y-=image.sprite_perso[i]->h-h;
       }
   }
}

if(signe==-1)
{
   for(h=image.sprite_perso[i]->h/4;h>0;h--)
   {
       if(getpixel(image.buffer_obst,perso[i]->x+image.sprite_perso[i]->w/2 , perso[i]->y+h )==makecol(255,0,0))
       {
           perso[i]->y+=h;
       }
   }
}

contact=1;
compteur++;

}
else{
        perso[i]->butt=0;
        perso[i]->vit_y/=4;
        perso[i]->vit_x/=4;

if(perso[i]->vit_x>0)temp=1;
if(perso[i]->vit_x<0)temp=-1;

if(((g>=0)&&(g<=10))||((g>=360-10)&&(g<=360)))
{
    mur=1;
    murr=1;
}
if((g>90+10)&&(g<270-10))
{
    mur=2;
    murr=1;
//    printf("%d\n", (int)g);
}

if(murr==1)
{
perso[i]->vit_y*=4;
}

   perso[i]->x+=-temp*r*cos((g+180)*rad)*(float)perso[i]->taille/1000;

   perso[i]->y-=signe*r*sin(-g*rad)*(float)perso[i]->taille/100;
   g=0;
   compteur++;
    contact=-1;
}
}
else if(d<=0){compteur=1000;}






}


}
    return contact;
}

int distance_contact(btmp image,BITMAP* obj_1,BITMAP* obj_2,float x_1,float y_1,float x_2,float y_2, float rayon)
{
    int contact=0;
if(pow((x_1+obj_1->w/2)-(x_2+obj_1->w/2), 2)+pow((y_1+obj_1->h/2)-(y_2+obj_2->h/2), 2)<=pow(rayon, 2))
{
  contact=1;

}
//printf("B %d\n", contact);
 return contact;

}


void detect_dead(t_perso** perso, t_editeur* ed,btmp image)
{
    int i,g;
    float dist=2*SCREEN_W;
    int good=-1;
    for(i=0;i<ed->nb_persos_max;i++)
    {
        if(perso[i]!=NULL)
        {
            if(perso[i]->life<=0)
            {
                perso[i]=NULL;
            }
        }


        if(perso[i]!=NULL)
        {
            if(perso[i]->x+image.sprite_perso[i]->w>-SCREEN_W/4&&perso[i]->x<SCREEN_W)
            {

            }
            else{ perso[i]=NULL;}
        }

        if(perso[i]!=NULL)
        {
            if(perso[i]->y+10*image.sprite_perso[i]->h>0&&perso[i]->y<SCREEN_H)
            {

            }
            else{ perso[i]=NULL;}
        }

        if(perso[i]!=NULL)
        {
            dist=2*SCREEN_W;

           if(perso[i]->x+image.sprite_perso[i]->w<0)
           {
//               if(perso[i]->y>SCREEN_H-image.sprite_perso[i]->h)
//               {

for(g=0;g<ed->nb_persos_max;g++)
{
//

    if(perso[g]!=NULL)
    {
        if(g!=i)
        {
          if(perso[g]->x<dist)
          {dist=perso[g]->x;
          good=g;
          }
        }
    }
}
perso[i]->y=perso[good]->y;
perso[i]->vit_y=0;
               perso[i]->contact=1;
//               }
           }
        }

    }



}



void collision_persos(t_perso** perso, t_editeur* ed, btmp image)
{
    float r=ed->fps_ratio;
    int i,j,g;
    int d_x;
    int cont=0;
    for(i=0;i<ed->nb_persos_max;i++)
    {
        if(perso[i]!=NULL)
        {
          for(j=0;j<ed->nb_persos_max;j++)
          {
              if(j!=i)
              {
                  if(perso[j]!=NULL)
                  {
  if(distance_contact(image,image.sprite_perso[j], image.sprite_perso[i], perso[j]->x+image.sprite_perso[j]->w/2,perso[j]->y+image.sprite_perso[j]->h/2,   perso[i]->x+image.sprite_perso[i]->w/2,perso[i]->y+image.sprite_perso[i]->h/2,(float)image.sprite_perso[i]->w/2 )==1)
  {
d_x=(perso[i]->x-perso[j]->x)/30;
cont=0;
if(perso[i]->idle==1)
{
if(perso[i]->x-perso[j]->x>-(float)image.sprite_perso[i]->w/3&&perso[i]->x-perso[j]->x<(float)image.sprite_perso[i]->w/4){perso[i]->x+=(rand()%3-1)*image.sprite_perso[i]->w/10;printf("F");}
}
else{
//        do{
//
//       perso[i]->x+=r*(perso[i]->x-perso[j]->x)/30;
//if((perso[i]->x-perso[j]->x)<-image.sprite_perso[i]->w/2) cont=1;
//if((perso[i]->x-perso[j]->x)>image.sprite_perso[i]->w/2) cont=1;
//        }while(cont==0);
//        cont=0;
//
//g=0;
//do{
//
//       perso[i]->y-=r*(perso[i]->y-perso[j]->y)/30;
//       g++;
//if((perso[i]->y-perso[j]->y)<-image.sprite_perso[i]->h/2) cont=1;
//if((perso[i]->y-perso[j]->y)>image.sprite_perso[i]->h/2) cont=1;
//if(g>10)cont=1;
//        }while(cont==0);
//

}


  }
                  }
              }
          }


        }


    }



}

void collision_cote(t_perso** perso, t_editeur* ed,btmp image,int i)
{

}


void detect_piece(t_perso** perso, t_objet** piece,t_editeur* ed,btmp image)
{
   int i,j;
   for(i=0;i<ed->nb_persos_max;i++)
   {
    if(perso[i]!=NULL)
    {
        for(j=0;j<ed->nb_pieces_max;j++)
        {
            if(piece[j]!=NULL)
            {
                if(piece[j]->x-ed->scroll_x>0&&piece[j]->x-ed->scroll_x<SCREEN_W)
                {
//                   printf("X: %f,   Y: %f\n",piece[j]->x,piece[j]->y );
                    if( distance_contact(image,image.sprite_perso[i],image.piece_collision,perso[i]->x,perso[i]->y,piece[j]->x-ed->scroll_x,piece[j]->y,(image.sprite_perso[i]->h/2+image.piece_collision->w))==1 )
                    {
                        ed->pieces+=piece[j]->bonus;
                        piece[j]=NULL;

                    }
                }
            }
        }
    }
   }
}


void detect_bloc(t_perso** perso, t_objet** bloc,t_editeur* ed,btmp image)
{
   int i,j;
   for(i=0;i<ed->nb_persos_max;i++)
   {
    if(perso[i]!=NULL)
    {
        for(j=0;j<ed->nb_blocks_max;j++)
        {
            if(bloc[j]!=NULL)
            {
               if(bloc[j]->y+image.block->h<=perso[i]->y)
               {
                if(bloc[j]->x-ed->scroll_x>0&&bloc[j]->x-ed->scroll_x<SCREEN_W)
                {
                    if( distance_contact(image,image.sprite_perso[i],image.block,perso[i]->x,perso[i]->y,bloc[j]->x-ed->scroll_x,bloc[j]->y,(image.sprite_perso[i]->h/2+image.block->w))==1 )
                    {
                        bonus(perso, ed,bloc[j]->bonus);
                        bloc[j]=NULL;
                    }
                }
            }
            }
        }
    }
   }
}




void obj_inter_cote(t_perso** perso, t_objet** nuage, int nb, int nb_persos)
{
    int i,j;
    for(i=0;i<nb;i++)
    {
        if(nuage[i]!=NULL)
        {
            nuage[i]->cote=0;
           for(j=0;j<nb_persos;j++)
           {
               if(perso[j]!=NULL)
               {
                 if(perso[j]->x>nuage[i]->x)
                 {
                     nuage[i]->cote=1; //va � droite
                   if(perso[j]->direction==-1)
                   {
                      nuage[i]->cote=2;//s'arr�te � droite
                   }
                 }
                 else{


                        nuage[i]->cote=-1; //va � gauche
                       if(perso[j]->direction==1)
                   {
                      nuage[i]->cote=-2; //s'arr�te � cgauche
                   }

                        }  //droite


                        if(perso[j]->y>nuage[i]->y)
                        {
                            nuage[i]->y_cot=1;
                        }
                        else{nuage[i]->y_cot=-1;}
               }

           }


        }
    }


}




void take_warp(t_perso** perso,t_editeur* ed, t_pipe** pipe,btmp image)
{
  int i,j;

  if(perso[ed->perso_select]!=NULL)
  {
      if(perso[ed->perso_select]->cooldown<=0)
      {

     for(i=0;i<ed->nb_pipes_max;i++)
     {
         if(pipe[i]!=NULL)
         {
//             if(pipe[i]->x-ed->scroll_x)
//              {


             if(pipe[i]->y_1>perso[ed->perso_select]->y)
             {
                 if(distance_contact(image,
                                      image.sprite_perso[ed->perso_select],
                                       image.pipe,perso[ed->perso_select]->x,
                                       perso[ed->perso_select]->y,
                                       pipe[i]->x_1-ed->scroll_x,
                                        pipe[i]->y_1,
                                        image.pipe->h/2+image.sprite_perso[ed->perso_select]->h/2)==1)
                 {
                    ed->scroll_x=pipe[i]->x_2;
                    perso[ed->perso_select]->x=0;
                  perso[ed->perso_select]->y=pipe[i]->y_2-image.sprite_perso[ed->perso_select]->h;
                  set_cooldown(perso,ed->perso_select,200);
                  break;
                 }
             }


                if(pipe[i]->y_2>perso[ed->perso_select]->y)
             {
                 if(distance_contact(image,
                                      image.sprite_perso[ed->perso_select],
                                       image.pipe,perso[ed->perso_select]->x,
                                       perso[ed->perso_select]->y,
                                       pipe[i]->x_2-ed->scroll_x,
                                        pipe[i]->y_2,
                                        image.pipe->h/2+image.sprite_perso[ed->perso_select]->h/2)==1)
                 {
                    ed->scroll_x=pipe[i]->x_1;
                    perso[ed->perso_select]->x=0;
                  perso[ed->perso_select]->y=pipe[i]->y_1-image.sprite_perso[ed->perso_select]->h;
                    set_cooldown(perso,ed->perso_select,200);
                  break;
                 }
             }


//         }
         }
     }

  }
  }





}




int arrivee(t_perso** perso, t_editeur* ed, btmp image, int x_arrivee)
{
    int i;
    if(ed->scroll_x+SCREEN_W>x_arrivee)
    {
        for(i=0;i<ed->nb_persos_max;i++)
        {
            if(perso[i]!=NULL)
            {
                if(ed->scroll_x+perso[i]->x+image.sprite_perso[i]->w>=x_arrivee)
                {

                    return 1;
                }
            }
        }
    }


return 0;
}

int check_arrivee(t_perso** perso,t_editeur* ed, btmp image, int check)
{
    int i,g;
     float r=ed->fps_ratio;
    if(check==1)
    {
       for(i=0;i<ed->nb_persos_max;i++)
       {
           if(perso[i]!=NULL)
           {
//                perso[i]->x-=r*ed->vit_x_max*5;
//                 perso[i]->y=SCREEN_H/2-SCREEN_H/60;
                if(ed->scroll_x<SCREEN_W)
                {


                  return 0;

                }
           }
       }
    }

return 1;
}
