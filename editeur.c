#include "fonctions.h"




void init_editeur(t_editeur* ed)
{
    ed->nb_persos_max=5;
    ed->perso_select=0;
    ed->acc_x=(3*(float)SCREEN_W/50000);
//    ed->acc_y=2*(float)SCREEN_H/2500;
ed->acc_y=9.81*40/((float)SCREEN_H*6);
    ed->sprite_changed=0;
    ed->life_max=50;
    ed->vit_x_max=3;
    ed->vit_y_max=4;
    ed->scroll_x=0;
    ed->scroll_x_old=0;
    ed->nb_blocks_max=50;
    ed->nb_b_blocks_max=500;
    ed->nb_para_max=50;
    ed->nb_goom_max=50;

    ed->nb_pieces_max=200;
    ed->compteur_max=5;
    ed->pieces=0;
    ed->pieces_old=0;
    ed->weather=5;
    ed->weather_max=50;
    ed->nb_boo=0;
    ed->nb_boo_max=5;
    ed->nb_pipes_max=5;
    ed->fps_max=80;
    ed->fps_ori=100;
    ed->fps=60;
    ed->key_cooldown=0;
    ed->fps_ratio=1;
    ed->show_map=0;
    p_controls(ed);
    ed->cursor=0;
    ed->obj_falling_speed=4;
    ed->nb_fire_max=3;
    ed->nb_antig_max=20;


    ed->backup=malloc(ed->nb_persos_max*sizeof(t_perso*));
    int i;
    for(i=0;i<ed->nb_persos_max;i++)
    {
      ed->backup[i]=NULL;
       create_perso(ed->backup,ed,i);
       ed->backup[i]->acc_y=ed->acc_y;


    }
//    ed->fps=0;

}


void create_perso(t_perso** perso, t_editeur* ed,int i)
{
  int g;

   if(perso[i]!=NULL)
   {

   }
   else{
    perso[i]=malloc(sizeof(t_perso));
     perso[i]->x=SCREEN_W/2;
     perso[i]->y=SCREEN_H/2;
     perso[i]->vit_x=0;
     perso[i]->vit_y=0;
     perso[i]->idle=1;
     perso[i]->land=1;
//     perso[i]->poids=3*(float)SCREEN_H/(5000*18);
perso[i]->poids=20;
     perso[i]->acc_x=0;
     perso[i]->acc_y=0;
     perso[i]->life_max=50;
     perso[i]->life=perso[i]->life_max/2;
     perso[i]->life_old=perso[i]->life;

     perso[i]->pieces=0;
     perso[i]->cooldown=0;
     perso[i]->cooldown_sprite=0;
     perso[i]->cooldown_life=0;
     perso[i]->action=0;
     perso[i]->taille=1;
     perso[i]->contact=0;
     perso[i]->direction=1;
     perso[i]->butt=0;
     perso[i]->sprite_etat=0;
      perso[i]->cooldown_sol=0;
      perso[i]->compteur_idle=0;
      perso[i]->sprite_etat=0;
      perso[i]->etat=0;
      perso[i]->character=0;

     for(g=0;g<360;g++)perso[i]->deg[g]=0;


//     perso[i]->couleur=makecol(100,100,100);
     switch(i)
     {
        case 0: perso[i]->couleur=makecol(0,255,0);break;
        case 1: perso[i]->couleur=makecol(0,100,255);break;
         case 2:perso[i]->couleur=makecol(100,255,0);break;
         case 3:perso[i]->couleur=makecol(100,0,100);break;
         case 4:perso[i]->couleur=makecol(100,0,255);break;

         default:perso[i]->couleur=makecol(100,100,100);break;
     }

   }






}



void create_piece(t_objet** piece,t_editeur* ed, btmp image)
{
    int i;
    for(i=0;i<ed->nb_pieces_max;i++)
    {
        piece[i]=malloc(sizeof(t_objet));
            piece[i]->x=rand()%image.bckg->w;
            piece[i]->y=rand()%(6*SCREEN_H/8)+SCREEN_H/8;
            piece[i]->sprite_etat=0;
            piece[i]->compteur=ed->compteur_max;
            piece[i]->bonus=1;
            piece[i]->type=-1;
//            printf("X: %f,   Y: %f\n",piece[i]->x,piece[i]->y );

    }


}



int creer_boo(t_objet** boo, int nb_max,int interactif)
{
  int i,j;

for(i=0;i<nb_max;i++)
{
  if(boo[i]!=NULL)
  {
j=i;
  }
  else{
        boo[i]=malloc(sizeof(t_objet));
     boo[i]->compteur=rand()%SCREEN_W;
     boo[i]->y=0;
     boo[i]->cs=rand()%360;
     boo[i]->vit_y=rand()%100;
     boo[i]->vit_x=rand()%100;
     boo[i]->interactif=interactif;
    return i;
  }
}

return j;
   }


void editeur_fps_adapt(t_editeur* ed)
{
    ed->fps_ratio=ed->fps_ori/ed->fps;

}


void p_controls(t_editeur* ed)
{
    ed->tab[0][0]=key[KEY_LEFT];
    ed->tab[0][1]=key[KEY_RIGHT];
    ed->tab[0][2]=key[KEY_UP];
    ed->tab[0][3]=key[KEY_DOWN];
    ed->tab[0][4]=key[KEY_0_PAD];
     ed->tab[0][5]=key[KEY_1_PAD];

     ed->tab[1][0]=key[KEY_A];
    ed->tab[1][1]=key[KEY_D];
    ed->tab[1][2]=key[KEY_W];
    ed->tab[1][3]=key[KEY_S];
      ed->tab[1][4]=key[KEY_E];
       ed->tab[1][5]=key[KEY_Q];

     ed->tab[2][0]=key[KEY_J];
    ed->tab[2][1]=key[KEY_L];
    ed->tab[2][2]=key[KEY_I];
    ed->tab[2][3]=key[KEY_K];
      ed->tab[2][4]=key[KEY_O];
      ed->tab[2][5]=key[KEY_U];

    ed->tab[3][0]=key[KEY_F];
    ed->tab[3][1]=key[KEY_H];
    ed->tab[3][2]=key[KEY_T];
    ed->tab[3][3]=key[KEY_G];
      ed->tab[3][4]=key[KEY_Y];
      ed->tab[3][5]=key[KEY_R];

    ed->tab[4][0]=key[KEY_4_PAD];
    ed->tab[4][1]=key[KEY_6_PAD];
    ed->tab[4][2]=key[KEY_8_PAD];
    ed->tab[4][3]=key[KEY_5_PAD];
      ed->tab[4][4]=key[KEY_9_PAD];
      ed->tab[4][5]=key[KEY_7_PAD];

}



t_editeur* ALLOCATION_MONDE(btmp image,t_perso** perso,t_objet** pipe,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** nuage,t_objet** boo,t_objet** piece,t_objet** fireball,t_objet** antig,t_fichier fichier,t_effect* effect, int c_map)
{
    int i;
  char path[50];


if(c_map==-1){
//        play_sample(effect->title,255,128,1000,1);
}
else{
        stop_sample(effect->title);
        if(effect->level!=NULL)
        stop_sample(effect->level);
  char s[50];
sprintf(s,"sound/map_%d.wav", c_map);
effect->level=NULL;
  effect->level=load_sample(s);
  if(effect->level!=NULL)
  {
  }
  else{effect->level=effect->title;}
//play_sample(effect->level,128,128,1000,1);
}


      t_editeur* ed=malloc(sizeof(t_editeur));
      init_editeur(ed);


    ////////////////////////////////////////////////////////////////
//////////alloc des pipes
////////////////////////////////////////////////
////////////////////////////////////////////////
for(i=0;i<ed->nb_pipes_max;i++)
{
    pipe[i]=malloc(sizeof(t_pipe));
    pipe[i]=NULL;
}
sprintf(path,"data/pipes/bloc_map_%d.txt", c_map);
//creer_pipe(pipe, ed, 100,200, 500,300);
//creer_pipe(pipe, ed, 0,SCREEN_H-image.pipe->h, image.bckg->w-10*image.pipe->w,SCREEN_H-image.pipe->h);
charger_pipes(pipe,ed, fichier,ed->nb_pipes_max,path);

////////Alloc des blocs
for(i=0;i<ed->nb_blocks_max;i++)
{
    bloc[i]=malloc(sizeof(t_objet));
    bloc[i]=NULL;
}
sprintf(path,"data/bloc/bloc_map_%d.txt", c_map);
charger_blocs(bloc,ed, fichier,ed->nb_blocks_max,path);


////////Alloc des blocs breakable
for(i=0;i<ed->nb_b_blocks_max;i++)
{
    b_bloc[i]=malloc(sizeof(t_objet));
    b_bloc[i]=NULL;
}
sprintf(path,"data/b_bloc/bloc_map_%d.txt", c_map);
charger_blocs(b_bloc,ed,fichier,ed->nb_b_blocks_max, path);


////////Alloc des blocs breakable
for(i=0;i<ed->nb_para_max;i++)
{
    para[i]=malloc(sizeof(t_objet));
    para[i]=NULL;
}
sprintf(path,"data/paratroopa/bloc_map_%d.txt", c_map);
charger_blocs(para,ed,fichier,ed->nb_para_max, path);


////////Alloc des gommbaaazazqdsss
for(i=0;i<ed->nb_goom_max;i++)
{
    goom[i]=malloc(sizeof(t_objet));
    goom[i]=NULL;
}
sprintf(path,"data/goomba/bloc_map_%d.txt", c_map);
charger_blocs(goom,ed,fichier,ed->nb_goom_max, path);

for(i=0;i<ed->nb_goom_max;i++)
{
    if(goom[i]!=NULL)
    {
        goom[i]->y_ori=goom[i]->x;
    }
}



////////Alloc des fireballs
for(i=0;i<ed->nb_persos_max*ed->nb_fire_max;i++)
{
    fireball[i]=malloc(sizeof(t_objet));
    fireball[i]=NULL;
}



////////Alloc des blocs breakable
for(i=0;i<ed->nb_antig_max;i++)
{
    antig[i]=malloc(sizeof(t_objet));
    antig[i]=NULL;
}
sprintf(path,"data/antig/bloc_map_%d.txt", c_map);
charger_blocs(antig,ed,fichier,ed->nb_antig_max, path);

for(i=0;i<ed->nb_antig_max;i++)
{
    if(antig[i]!=NULL)
    {
        antig[i]->type=-1;
    }
}


////////Alloc des nuages

    for(i=0;i<ed->weather_max;i++)
   {
     nuage[i]=malloc(sizeof(t_objet));
     nuage[i]=NULL;
   }

   for(i=0;i<ed->weather;i++)
   {
     nuage[i]=malloc(sizeof(t_objet));
     nuage[i]->compteur=rand()%SCREEN_W;
     nuage[i]->y=0;
     nuage[i]->cs=rand()%360;
     nuage[i]->vit_y=rand()%100;
     nuage[i]->vit_x=rand()%100;
     nuage[i]->interactif=0;
   }



ini_nuages(nuage,ed);



//////////Alloc des boos

    for(i=0;i<ed->nb_boo_max;i++)
   {
     boo[i]=malloc(sizeof(t_objet));
     boo[i]=NULL;
   }

   for(i=0;i<ed->nb_boo;i++)
   {
     boo[i]=malloc(sizeof(t_objet));
     boo[i]->compteur=rand()%SCREEN_W;
     boo[i]->y=0;
     boo[i]->cs=rand()%360;
     boo[i]->vit_y=rand()%100;
     boo[i]->vit_x=rand()%100;
     boo[i]->interactif=1;
     boo[i]->sprite_etat=0;
   }



////////////Alloc des pieces
//for(i=0;i<ed->nb_pieces_max;i++)
//{
//    piece[i]=malloc(sizeof(t_objet));
//    piece[i]=NULL;
//
//}
//create_piece(piece,ed,image);


////////Alloc des pieces
for(i=0;i<ed->nb_pieces_max;i++)
{
    piece[i]=malloc(sizeof(t_objet));
    piece[i]=NULL;
}
sprintf(path,"data/pieces/bloc_map_%d.txt", c_map);
charger_blocs(piece,ed, fichier,ed->nb_pieces_max,path);
for(i=0;i<ed->nb_pieces_max;i++)
{
    if(piece[i]!=NULL)
    {
        piece[i]->type=-1;
    }
}

////////////////Alloc des persos

for(i=0;i<ed->nb_persos_max;i++)
{
    perso[i]=malloc(sizeof(t_perso));
    perso[i]=NULL;
}




return ed;
}
