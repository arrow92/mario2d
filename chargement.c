#include "fonctions.h"


t_fichier maps(t_fichier fichier,int Mapx,int Mapy,btmp image,t_cliff cliff)
{

char n_falaise[50];

sprintf(n_falaise,"levels/falaise_level_%d_%d.txt", Mapx,Mapy);

fichier.falaise=NULL;


fichier.falaise=charger_fichier(fichier.falaise, n_falaise);


fscanf(fichier.falaise,"%d", &cliff.nombre);

if(cliff.nombre!=0)
{
  int falaisetraite;
for(falaisetraite=0;falaisetraite<cliff.nombre;falaisetraite++)
{
    fscanf(fichier.falaise,"%d %d %f",&cliff.x[falaisetraite],&cliff.y[falaisetraite],&cliff.rot[falaisetraite]);
    clear_to_color(image.rotation,makecol(255,0,255));

image=charge_obstacle_turned(image,image.buffer,image.cliff_long,image.rotation,cliff.x[falaisetraite],cliff.y[falaisetraite],SCREEN_W,SCREEN_H,cliff.rot[falaisetraite]);

}

}


rest(100);

fclose(fichier.falaise);

return fichier;
}


FILE* charger_fichier(FILE* file, char* chaine)
{
    int error;
    do{
file=fopen(chaine,"r");
error=0;
if(file==NULL)
{
    fclose(file);
    file=fopen(chaine,"w");
    fprintf(file,"0");
    fclose(file);
    error=1;
}
}while(error==1);

return file;
}

void charger_blocs(t_objet** bloc,t_editeur* ed, t_fichier fichier,int nb_max, char* path)
{
    int nb=0;
    int i;


nb=nb_elements_fichier(path,3);

fichier.bloc=fopen(path, "r");
//fscanf(fichier.bloc,"%d", &nb);

for(i=0;i<nb_max;i++)
{
    bloc[i]=malloc(sizeof(t_objet));
    bloc[i]=NULL;
}

for(i=0;i<nb;i++)
{
    if(i<nb_max)
    {
        bloc[i]=malloc(sizeof(t_objet));
        bloc[i]->sprite_etat=0;
        bloc[i]->compteur=0;
        bloc[i]->vit_x=0;
        bloc[i]->vit_y=0;
        bloc[i]->cs=1;
        bloc[i]->cx=1;

    }
}

for(i=0;i<nb;i++)
{
    if(i<nb_max)
    {
        fscanf(fichier.bloc, "%f %f %d", &bloc[i]->x,&bloc[i]->y, &bloc[i]->bonus);
    if(bloc[i]->bonus==42)
    {
        bloc[i]->type=1;

    }
    else{bloc[i]->type=0;}
      bloc[i]->y_ori=bloc[i]->y;
    }
//printf("%s, %d\n", path,bloc[i]->bonus);
}

fclose(fichier.bloc);
}


void charger_pipes(t_objet** bloc,t_editeur* ed, t_fichier fichier,int nb_max, char* path)
{
    int nb=0;
    int i;


nb=nb_elements_fichier(path,4);

fichier.bloc=fopen(path, "r");
//fscanf(fichier.bloc,"%d", &nb);

for(i=0;i<nb_max;i++)
{
    bloc[i]=malloc(sizeof(t_objet));
    bloc[i]=NULL;
}

for(i=0;i<nb;i++)
{
    if(i<nb_max)
    {
        bloc[i]=malloc(sizeof(t_objet));
        bloc[i]->sprite_etat=0;
        bloc[i]->compteur=0;
        bloc[i]->vit_x=0;
        bloc[i]->vit_y=0;
        bloc[i]->cs=1;
        bloc[i]->cx=1;
bloc[i]->cast=0;

    }
}

for(i=0;i<nb;i++)
{
    if(i<nb_max)
    {
        fscanf(fichier.bloc, "%f %f %f %f", &bloc[i]->x,&bloc[i]->y, &bloc[i]->x_2,&bloc[i]->y_2);
    if(bloc[i]->bonus==42)
    {
        bloc[i]->type=1;

    }
    else{bloc[i]->type=0;}
      bloc[i]->y_ori=bloc[i]->y;
    }
//printf("%s, %d\n", path,bloc[i]->bonus);
}

fclose(fichier.bloc);
}


void creer_bloc(t_objet** bloc,t_editeur* ed,int nb_max, int x,int y)
{
    int i;
    for(i=0;i<nb_max;i++)
    {
        if(bloc[i]!=NULL)
        {

        }
        else{
    bloc[i]=malloc(sizeof(t_objet));
    bloc[i]->x=x;
    bloc[i]->y=y;
     bloc[i]->sprite_etat=0;
        bloc[i]->compteur=0;
        bloc[i]->bonus=1;


        bloc[i]->vit_x=0;
        bloc[i]->vit_y=0;
        bloc[i]->cs=1;
bloc[i]->cooldown=0;
bloc[i]->cast=0;

          return nb_max;
        }
    }


}


void ini_nuages(t_objet**  nuage, t_editeur* ed)
{
   int i;


}


void creer_pipe(t_objet** pipe, t_editeur* ed, float x_1,float y_1, float x_2,float y_2)
{
   int i;
   for(i=0;i<ed->nb_pipes_max;i++)
   {
       if(pipe[i]!=NULL)
       {

       }
       else
        {
        pipe[i]=malloc(sizeof(t_objet));
       pipe[i]->x=x_1;
       pipe[i]->x_2=x_2;
       pipe[i]->y=y_1;
       pipe[i]->y_2=y_2;
break;
       }
   }



}


t_editeur* LOAD_NEXT_MAP(btmp image,t_perso** perso,t_objet** pipe,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** nuage,t_objet** boo,t_objet** piece,t_objet** fireball,t_objet** antig,t_fichier fichier,t_effect* effect, int c_map,t_editeur* ed)
{
       chgmt_map(image,perso,ed,pipe,bloc,nuage,boo,piece);
//    if(c_map<10)c_map++;


    ed=ALLOCATION_MONDE(image,perso,pipe,bloc,b_bloc,para,goom,nuage,boo,piece,fireball,antig,fichier,effect,c_map);


  return ed;
}



int nb_elements_fichier(char* path, int mots_ligne)
{
    int compteur=0;
    int i=0;
    int j;
    int blop=0;
   FILE* f=NULL;
   f=fopen(path,"r");

   if(f!=NULL)
   {

   }
   else{f=fopen(path,"w"); fclose(f);f=fopen(path,"r");}

    while(!feof(f))
    {
        for(j=0;j<mots_ligne;j++)
        {
          fscanf(f,"%d", &blop);
        }
        compteur++;
    }
    fclose(f);

if(compteur>0)compteur--;

  return compteur;
}


void menu(btmp image,t_perso** perso,t_objet** pipe,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** nuage,t_objet** boo,t_objet** piece,t_fichier fichier,t_editeur* ed,t_effect* effect)
{


    int i;
    int x=0;
    int compteur=0;
    float vit_y=1;
    float vit_x=1;
    float vit_y_max=5;
    float v=vit_y;
    int start=0;
    int ke=0;
    int nb_b=30;


    for(i=0;i<ed->nb_b_blocks_max;i++)
    {
        x=i*(image.block->w/5);
        creer_bloc(b_bloc,ed,ed->nb_b_blocks_max, SCREEN_W/2-image.block->w/10,SCREEN_H/2-image.block->h/2);
        b_bloc[i]->bonus=rand()%30+1;
         b_bloc[i]->cs=1;
         b_bloc[i]->type=0;
    }

    do{
 clock_t timer=clock();



camera_clic_move(ed,image.bckg);

       for(i=0;i<ed->nb_b_blocks_max;i++)
       {

           if(b_bloc[i]!=NULL)
           {

    b_bloc[i]->y+=b_bloc[i]->cs*i*0.0981;
    b_bloc[i]->x+= b_bloc[i]->bonus/4;

    if(b_bloc[i]->y+image.block->h>SCREEN_H)
    {
     b_bloc[i]->cs=-b_bloc[i]->cs;
    }

     if(b_bloc[i]->y<0)
    {
    b_bloc[i]->cs=-b_bloc[i]->cs;
    }

     if(b_bloc[i]->x+image.block->w/5<0)
    {
     b_bloc[i]->bonus=-b_bloc[i]->bonus;
    }

      if(b_bloc[i]->x>SCREEN_W)
    {
     b_bloc[i]->bonus=-b_bloc[i]->bonus;
    }



           }
       }

        clear_to_color(image.buffer,makecol(255,0,255));
         image.buffer=background_scroll(image,ed);

       image=block(image,ed, bloc,image.block,image.block->w/5,image.block->h,ed->nb_blocks_max,0);
       if(ed->nb_b_blocks_max>30)
       {

           image=block(image,ed, b_bloc,image.b_block,image.block->w/5,image.block->h,30,0);

       }


     else image=block(image,ed, b_bloc,image.block,image.block->w/5,image.block->h,ed->nb_b_blocks_max,0);

if(key[KEY_T])
{
    if(ke==0)
    {
    ke=1;
   start++;
    }

}
if(!key[KEY_T])
{
    ke=0;
}



//sprite_next_piece(b_bloc,ed,ed->nb_b_blocks_max,10*ed->compteur_max,4);


    if(start==1)
    {draw_sprite(image.buffer,image.title,0,0);
   image.title_1=multicolor(image.title_1);
    draw_sprite(image.buffer,image.title_1,SCREEN_W/2-image.title_1->w/2,SCREEN_H/2+image.title_1->h/2);}

 draw_sprite(screen,image.buffer,0,0);


detect_dead(perso,ed,image);

do{
   rest(1);

}
while(1/(((double)clock() - timer) / CLOCKS_PER_SEC)>ed->fps_max);



ed->fps=1/(((double)clock() - timer) / CLOCKS_PER_SEC);


    }while(start<2);

}
