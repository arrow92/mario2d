#include "fonctions.h"




btmp charge_obstacle(btmp image,BITMAP* bmp, BITMAP* pic, float x, float y, int couleur, int draw)
{
 int i,j;
 int c;

 if(draw==1)
    draw_sprite(image.buffer,pic,x,y);
 for(i=0;i<pic->w;i++)
 {
     for(j=0;j<pic->h;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }




 return image;
}


btmp charge_obstacle_intel(btmp image,BITMAP* bmp, BITMAP* pic,t_perso** perso, t_editeur* ed, float x, float y, int couleur, int draw)
{
 int i,j;
 int c;
 int g;

 if(draw==1)
    draw_sprite(image.buffer,pic,x,y);

    for(g=0;g<ed->nb_persos_max;g++)
    {
        if(perso[g]!=NULL)
        {
//               image.sprite_perso[i]->h/5;j<4*image.sprite_perso[i]->h/5
         for(i=perso[g]->x;i<perso[g]->x+perso[g]->taille*3*image.sprite_perso[g]->w/5;i++)
 {
     for(j=perso[g]->y-perso[g]->taille*image.sprite_perso[g]->h/5;j<perso[g]->y+perso[g]->taille*image.sprite_perso[g]->h;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur); //gauche
        }
     }

 }

    for(i=perso[g]->x+perso[g]->taille*3*image.sprite_perso[g]->w/5;i<perso[g]->x+perso[g]->taille*image.sprite_perso[g]->w;i++)
 {
     for(j=perso[g]->y-perso[g]->taille*image.sprite_perso[g]->h/5;j<perso[g]->y+perso[g]->taille*image.sprite_perso[g]->h;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }


     for(i=perso[g]->x;i<perso[g]->x+perso[g]->taille*image.sprite_perso[g]->w;i++)
 {

     for(j=perso[g]->y-perso[g]->taille*image.sprite_perso[g]->h/8;j<perso[g]->y+perso[g]->taille*image.sprite_perso[g]->h/8;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }

   for(i=perso[g]->x;i<perso[g]->x+perso[g]->taille*image.sprite_perso[g]->w;i++)
 {
     for(j=perso[g]->y+perso[g]->taille*7*image.sprite_perso[g]->h/8;j<perso[g]->y+perso[g]->taille*8*image.sprite_perso[g]->h/6;j++)
     {
        if(getpixel(pic,i,j)!=makecol(255,0,255))
        {
            putpixel(bmp,i+x,j+y,couleur);
        }
     }

 }


        }
    }





 return image;
}



int contact_sol_beta(t_perso** perso,btmp image,int i, t_editeur* ed)
{
float r=ed->fps_ratio;
   int contact=0;
   int j,g;
   int c;
   float poids=(float)SCREEN_H/(5000*20);
if(perso[i]!=NULL)
{
    for(g=0;g<3*r;g++)
       {
   for(j=image.perso[0]->w/24;j<image.perso[0]->w/12;j+=10)
   {

//       putpixel(screen,perso[i]->x+j,perso[i]->y-image.perso[0]->h/60+image.perso[0]->h/4-g,makecol(255,0,0));
        c=getpixel(image.buffer_obst,perso[i]->x+j,perso[i]->y-image.perso[0]->h/60+image.perso[0]->h/4-g);
       if(c==makecol(255,0,0))

       {

if(g==0){
           perso[i]->vit_y=0;
           perso[i]->acc_y=0;
           contact=1;
}

else{
   perso[i]->y-=g/2;

}

       }



          if(j>image.perso[0]->w/24 &&j<image.perso[0]->w/12-10)
        {
//             putpixel(screen,perso[i]->x+j,perso[i]->y+image.perso[0]->h/60-g,makecol(0,255,0));
       if(getpixel(image.buffer_obst,perso[i]->x+j,perso[i]->y+image.perso[0]->h/60-g)==makecol(255,0,0))
       {

            perso[i]->vit_y-=g*pow(pow(perso[i]->vit_y,2),1/2)/4;
           perso[i]->acc_y=0;
       }
    }

   }
   }
}


    return contact;
}



int* angle_moyen(t_perso** perso, float g, btmp image,int tab[3], int i, t_editeur* ed)
{
int c,d; float r=ed->fps_ratio;
float rad=0.0174532925;
int signe=1;
if(ed->acc_y<0)signe=-1;
int temp=1;

  for(g=0;g<360;g++)
{


perso[i]->deg[(int)g]=0;



//if((int)g%10==0)putpixel(screen,perso[i]->x+perso[i]->taille*image.sprite_perso[i]->w/2 +cos(g*rad)*2*40*perso[i]->taille/3+r*perso[i]->vit_x, perso[i]->y+perso[i]->taille*image.sprite_perso[i]->h/2+40*sin(-g*rad)*perso[i]->taille*signe +r*perso[i]->vit_y,makecol(255,0,0) );
 c=getpixel(image.buffer_obst,perso[i]->x+perso[i]->taille*image.sprite_perso[i]->w/2 +cos(g*rad)*2*40*perso[i]->taille/3+r*perso[i]->vit_x, perso[i]->y+perso[i]->taille*image.sprite_perso[i]->h/2+40*sin(-g*rad)*perso[i]->taille*signe +r*perso[i]->vit_y );
 if(c==(makecol(255,0,0)))
 {

     perso[i]->deg[(int)g]=1;
     tab[0]++;
     tab[1]+=g;

 }

 if(c==makecol(0,255,0))
 {
life_change(perso,ed,-25,i);
//    perso[i]->life-=0.005*r;
 }

 if(c==makecol(0,100,0))
 {
     if(ed->acc_y>0)
     {
         ed->acc_y=-ed->acc_y;

     }

 }

  if(c==makecol(0,110,0))
 {
     if(ed->acc_y<0)
     {


     ed->acc_y=-ed->acc_y;

     }
 }


// if(c==makecol(0,90,0))
// {
////  perso[i]->life-=20*r;
// }

}
  return tab[3];
}


int contact_sol(t_perso** perso,btmp image,int i, t_editeur* ed)
{
float r=ed->fps_ratio;
   int contact=0;
   float j,g;
   int c;
   int compteur;
   int sortir=0;
   float poids=9.81/20;
   float rad=0.0174532925;
   int d=0;
   int rate=0;
   int tab[3]={0};
   int mur=0;
   int murr=0;
   int h;
   int signe=1;
   if(ed->acc_y<0)signe=-1;


if(perso[i]!=NULL)
{

compteur=0;
int temp=1;

while(compteur<20)
{
      mur=0;
      murr=0;
    temp=1;
tab[3]=angle_moyen(perso, g,image,tab, i,ed);
d=tab[0];
rate=tab[1];
g=(float)rate/(float)d;


//printf("%d\n", (int)g);

if(d>0)
{
if((g>270-40)&&(g<270+40))
{
perso[i]->vit_y=0;
//    perso[i]->vit_y-=ed->vit_y_max;
     if(perso[i]->vit_y>0)perso[i]->vit_y=0;

if(signe==1)
{
   for(h=perso[i]->taille*3*image.sprite_perso[i]->h/4;h<perso[i]->taille*image.sprite_perso[i]->h-4;h++)
   {
       if(getpixel(image.buffer_obst,perso[i]->x+perso[i]->taille*image.sprite_perso[i]->w/2 , perso[i]->y+h )==makecol(255,0,0))
       {
           perso[i]->y-=image.sprite_perso[i]->h-h;
       }
   }
}

if(signe==-1)
{
   for(h=perso[i]->taille*image.sprite_perso[i]->h/4;h>0;h--)
   {
       if(getpixel(image.buffer_obst,perso[i]->x+perso[i]->taille*image.sprite_perso[i]->w/2 , perso[i]->y+h )==makecol(255,0,0))
       {
           perso[i]->y+=h;
       }
   }
}

contact=1;
compteur++;

}
else{
        perso[i]->butt=0;
        perso[i]->vit_y/=4;
        perso[i]->vit_x/=4;

if(perso[i]->vit_x>0)temp=1;
if(perso[i]->vit_x<0)temp=-1;

if(((g>=0)&&(g<=10))||((g>=360-10)&&(g<=360)))
{
    mur=1;
    murr=1;
}
if((g>90+10)&&(g<270-10))
{
    mur=2;
    murr=1;

}

if(murr==1)
{
perso[i]->vit_y*=4;
}

   perso[i]->x+=-temp*r*cos((g+180)*rad)*(40)/1000;

   perso[i]->y-=signe*r*sin(-g*rad)*(float)(40)/100;
   g=0;
   compteur++;
    contact=-1;
}
}
else if(d<=0){compteur=1000;}






}


}
    return contact;
}

int distance_contact(btmp image,BITMAP* obj_1,BITMAP* obj_2,float x_1,float y_1,float x_2,float y_2, float rayon)
{
    int contact=0;
if(pow((x_1+obj_1->w/2)-(x_2+obj_2->w/2), 2)+pow((y_1+obj_1->h/2)-(y_2+obj_2->h/2), 2)<=pow(rayon, 2))
{
  contact=1;

}
//printf("B %d\n", contact);
 return contact;

}


void detect_dead(t_perso** perso, t_editeur* ed,btmp image)
{
    int i,g;
    float dist=2*SCREEN_W;
    int good=-1;
    for(i=0;i<ed->nb_persos_max;i++)
    {
        if(perso[i]!=NULL)
        {
            if(perso[i]->life<=0)
            {
                perso[i]=NULL;
            }
        }


        if(perso[i]!=NULL)
        {
            if(perso[i]->x+image.sprite_perso[i]->w>-SCREEN_W/4&&perso[i]->x<SCREEN_W)
            {

            }
            else{ perso[i]=NULL;}
        }

        if(perso[i]!=NULL)
        {
            if(perso[i]->y+10*image.sprite_perso[i]->h>0&&perso[i]->y<SCREEN_H)
            {

            }
            else{ perso[i]=NULL;}
        }

        if(perso[i]!=NULL)
        {
            dist=2*SCREEN_W;

           if(perso[i]->x+image.sprite_perso[i]->w<0)
           {
//               if(perso[i]->y>SCREEN_H-image.sprite_perso[i]->h)
//               {

for(g=0;g<ed->nb_persos_max;g++)
{
//

    if(perso[g]!=NULL)
    {
        if(g!=i)
        {
          if(perso[g]->x<dist)
          {dist=perso[g]->x;
          good=g;
          }
        }
    }
}
perso[i]->y=perso[good]->y;
perso[i]->vit_y=0;
               perso[i]->contact=1;
//               }
           }
        }

    }



}



void collision_persos(t_perso** perso, t_editeur* ed, btmp image)
{
    float r=ed->fps_ratio;
    int i,j,g;
    int d_x;
    int cont=0;
    for(i=0;i<ed->nb_persos_max;i++)
    {
        if(perso[i]!=NULL)
        {
          for(j=0;j<ed->nb_persos_max;j++)
          {
              if(j!=i)
              {
                  if(perso[j]!=NULL)
                  {
  if(distance_contact(image,image.sprite_perso[j], image.sprite_perso[i], perso[j]->x+image.sprite_perso[j]->w/2,perso[j]->y+image.sprite_perso[j]->h/2,   perso[i]->x+image.sprite_perso[i]->w/2,perso[i]->y+image.sprite_perso[i]->h/2,(float)image.sprite_perso[i]->w/2 )==1)
  {
d_x=(perso[i]->x-perso[j]->x)/30;
cont=0;
if(perso[i]->idle==1)
{
if(perso[i]->x-perso[j]->x>-(float)image.sprite_perso[i]->w/3&&perso[i]->x-perso[j]->x<(float)image.sprite_perso[i]->w/4){perso[i]->x+=(rand()%3-1)*image.sprite_perso[i]->w/10;printf("F");}
}
else{


}


  }
                  }
              }
          }


        }


    }



}

void collision_cote(t_perso** perso, t_editeur* ed,btmp image,int i)
{

}


void detect_piece(t_perso** perso, t_objet** piece,t_editeur* ed,btmp image,int nb_max,t_effect* effect)
{
   int i,j;
   for(i=0;i<ed->nb_persos_max;i++)
   {
    if(perso[i]!=NULL)
    {
        for(j=0;j<nb_max;j++)
        {
            if(piece[j]!=NULL)
            {
                if(piece[j]->x-ed->scroll_x>0&&piece[j]->x-ed->scroll_x<SCREEN_W)
                {
//                   printf("X: %f,   Y: %f\n",piece[j]->x,piece[j]->y );
                    if( distance_contact(image,image.sprite_perso[i],image.piece_collision,perso[i]->x,perso[i]->y,piece[j]->x-ed->scroll_x,piece[j]->y,(perso[i]->taille*image.sprite_perso[i]->h/2+image.piece_collision->w))==1 )
                    {
bonus(perso, ed,i,piece[j]->bonus);
                        piece[j]=NULL;

                    }
                }
            }
        }
    }
   }
}


void detect_bloc(t_perso** perso, t_objet** bloc,t_editeur* ed,btmp image,t_effect* effect,int nb_max,int W,int H)
{
   int i,j;
   BITMAP* bk=create_bitmap(W,H);

   for(i=0;i<ed->nb_persos_max;i++)
   {
    if(perso[i]!=NULL)
    {
        if(perso[i]->cooldown<=0){
        for(j=0;j<nb_max;j++)
        {
            if(bloc[j]!=NULL)
            {
                if(bloc[j]->type==0)
{
            objet_passif(bloc,perso,image,ed,i,j,bk);
}
            }
           if(bloc[j]!=NULL)
            {
                 if(bloc[j]->type==1)
{
    objet_hostile(bloc,perso,image,ed,i,j,effect,bk);
}
            }

        }
    }
    }
   }
   destroy_bitmap(bk);
}


void objet_passif(t_objet** bloc,t_perso** perso,btmp image,t_editeur* ed,int i,int j,BITMAP* bk)
{
     if(bloc[j]->y+image.block->h-bloc[j]->vit_y*ed->fps_ratio<=perso[i]->y)
               {
                if(bloc[j]->x-ed->scroll_x>0&&bloc[j]->x-ed->scroll_x<SCREEN_W)
                {

                    if( distance_contact(image,image.sprite_perso[i],bk,perso[i]->x,perso[i]->y,bloc[j]->x-ed->scroll_x,bloc[j]->y,(image.sprite_perso[i]->h/2+bk->w/2))==1 )
                    {

                        bonus(perso, ed,i,bloc[j]->bonus);

                        ed->backup[i]->x=perso[i]->x;
                        ed->backup[i]->y=perso[i]->y;
                        ed->backup[i]->life=perso[i]->life_max;
                        ed->backup[i]->pieces=ed->scroll_x;
                        ed->backup[i]->acc_y=ed->acc_y;

//                        if(bloc[j]->bonus>=10)
//                        {
//                        set_cooldown(perso,i,20);
//                        }
//                        else
                        bloc[j]=NULL;




                    }



                }
            }



}

void objet_hostile(t_objet** bloc,t_perso** perso,btmp image,t_editeur* ed,int i,int j,t_effect* effect,BITMAP* bk)
{
int boucle=0;


                    if( distance_contact(image,image.sprite_perso[i],bk,perso[i]->x,perso[i]->y,bloc[j]->x-ed->scroll_x,bloc[j]->y,(perso[i]->taille*image.sprite_perso[i]->h/2+bk->w/2))==1 )
                    {
if(bloc[j]->bonus==666)
{
  if(bloc[j]->cast!=i)
  {
    life_change(perso,ed,-25,i);
    bloc[j]=NULL;
  }
}
else{
        if(bloc[j]!=NULL)
        {
                         if(bloc[j]->y<perso[i]->y)
               {
perso[i]->vit_x=(perso[i]->x+image.sprite_perso[i]->w/2-(bloc[j]->x+bk->w/2-ed->scroll_x))*20;
if(perso[i]->vit_x<0)
{
 if(perso[i]->vit_x<-ed->vit_x_max)
 {
     perso[i]->vit_x=-ed->vit_x_max;

 }
}
else if(perso[i]->vit_x>0)
    {
     if(perso[i]->vit_x>ed->vit_x_max)
 {
     perso[i]->vit_x=ed->vit_x_max;
 }
    }

perso[i]->vit_y=ed->vit_y_max/2;


life_change(perso,ed,-25,i);
               }

else
               {
                   perso[i]->butt=15;
                     bloc[j]->vit_y=ed->obj_falling_speed;
                                perso[i]->vit_y=-ed->vit_y_max;
                                stop_sample(effect->k_shell);
                                 play_sample(effect->k_shell,255,128,1000,0);
               }

}
                    }

                    }



}


void obj_obj_hostile(btmp image,t_perso** perso,t_objet** fire,t_objet** bloc,BITMAP* block,BITMAP* sprite,t_editeur* ed,t_effect* effect,int ra_b,int ra_s,int nb_max)
{
//int boucle=0;
int j,i;
BITMAP* sp=create_bitmap(sprite->w/ra_s,sprite->h);
BITMAP* b=create_bitmap(block->w/ra_b,block->h);

for(j=0;j<ed->nb_persos_max*ed->nb_fire_max;j++)
{
    if(fire[j]!=NULL)
    {
        for(i=0;i<nb_max;i++)
        {
            if(bloc[i]!=NULL)
            {
//                    printf("%f\n",bloc[i]->x);
//                     printf("%f\n",bloc[i]->y);
//                      printf("%f\n",fire[j]->x);
//                       printf("%f\n",fire[j]->y);

                    if( distance_contact(image,b,sp,bloc[i]->x,bloc[i]->y,fire[j]->x,fire[j]->y,(3*b->h/4+sp->h/2))==1 )
                    {
//                        rest(1000);
if(fire[j]->bonus==666)
{
bloc[i]->vit_y=ed->obj_falling_speed;
if(bloc[i]->type==-1)
{
    if(perso[fire[j]->cast]!=NULL)
     bonus(perso, ed,fire[j]->cast,bloc[i]->bonus);
     bloc[i]=NULL;
}

if(bloc[i]!=NULL)
{
if(bloc[i]->type==0)
{
    if(perso[fire[j]->cast]!=NULL)
     bonus(perso, ed,fire[j]->cast,bloc[i]->bonus);
     fire[j]=NULL;
//     bloc[i]=NULL;
}
}

if(bloc[i]!=NULL)
{
    if(fire[j]!=NULL)
    {
if(bloc[i]->type==1)
{
     fire[j]=NULL;
     stop_sample(effect->k_shell);
play_sample(effect->k_shell,255,128,1000,0);
}
}
}
//    bloc[i]=NULL;

break;
}



                    }
        }
        }
}
}
destroy_bitmap(sp);
destroy_bitmap(b);

}

void obj_inter_cote(t_perso** perso, t_objet** nuage, int nb, int nb_persos)
{
    int i,j;
    for(i=0;i<nb;i++)
    {
        if(nuage[i]!=NULL)
        {
            nuage[i]->cote=0;
           for(j=0;j<nb_persos;j++)
           {
               if(perso[j]!=NULL)
               {
                 if(perso[j]->x>nuage[i]->x)
                 {
                     nuage[i]->cote=1; //va � droite
                   if(perso[j]->direction==-1)
                   {
                      nuage[i]->cote=2;//s'arr�te � droite
                   }
                 }
                 else{


                        nuage[i]->cote=-1; //va � gauche
                       if(perso[j]->direction==1)
                   {
                      nuage[i]->cote=-2; //s'arr�te � cgauche
                   }

                        }  //droite


                        if(perso[j]->y>nuage[i]->y)
                        {
                            nuage[i]->y_cot=1;
                        }
                        else{nuage[i]->y_cot=-1;}
               }

           }


        }
    }


}




void take_warp(t_perso** perso,t_editeur* ed, t_objet** pipe,t_effect* effect,btmp image)
{
  int i,j;

  if(perso[ed->perso_select]!=NULL)
  {
      if(perso[ed->perso_select]->cooldown<=0)
      {

     for(i=0;i<ed->nb_pipes_max;i++)
     {
         if(pipe[i]!=NULL)
         {
//             if(pipe[i]->x-ed->scroll_x)
//              {


             if(pipe[i]->y>perso[ed->perso_select]->y)
             {
                 if(distance_contact(image,
                                      image.sprite_perso[ed->perso_select],
                                       image.pipe,perso[ed->perso_select]->x,
                                       perso[ed->perso_select]->y,
                                       pipe[i]->x-ed->scroll_x,
                                        pipe[i]->y,
                                        image.pipe->h/2+image.sprite_perso[ed->perso_select]->h/2)==1)
                 {
                      play_sample(effect->warp,125,128,1000,0);
perso[ed->perso_select]->vit_x=0;
perso[ed->perso_select]->vit_y=0;
take_pipe_a(pipe,  ed,image,  perso,1,pipe[i]->x,pipe[i]->y,ed->perso_select);

stop_sample(effect->warp);
                  play_sample(effect->warp,125,128,1000,0);
                  take_pipe_a(pipe,  ed,image,  perso,0,pipe[i]->x_2,pipe[i]->y_2,ed->perso_select);
 perso[ed->perso_select]->x=pipe[i]->x_2+image.pipe->w/2-ed->scroll_x-perso[ed->perso_select]->taille*image.sprite_perso[ed->perso_select]->h/2;


                  perso[ed->perso_select]->y=pipe[i]->y_2-perso[ed->perso_select]->taille*image.sprite_perso[ed->perso_select]->h;


set_cooldown_move_all(perso,ed->nb_persos_max, 10);
                  break;
                 }
             }


                if(pipe[i]->y_2>perso[ed->perso_select]->y)
             {
                 if(distance_contact(image,
                                      image.sprite_perso[ed->perso_select],
                                       image.pipe,perso[ed->perso_select]->x,
                                       perso[ed->perso_select]->y,
                                       pipe[i]->x_2-ed->scroll_x,
                                        pipe[i]->y_2,
                                        image.pipe->h/2+image.sprite_perso[ed->perso_select]->h/2)==1)
                 {
perso[ed->perso_select]->vit_x=0;
perso[ed->perso_select]->vit_y=0;
play_sample(effect->warp,125,128,1000,0);
                    take_pipe_a(pipe,  ed,image,  perso,1,pipe[i]->x_2,pipe[i]->y_2,ed->perso_select);


stop_sample(effect->warp);
                  play_sample(effect->warp,125,128,1000,0);

                    take_pipe_a(pipe,  ed,image,  perso,0,pipe[i]->x,pipe[i]->y,ed->perso_select);
 perso[ed->perso_select]->x=pipe[i]->x+image.pipe->w/2-ed->scroll_x-perso[ed->perso_select]->taille*image.sprite_perso[ed->perso_select]->h/2;

                  perso[ed->perso_select]->y=pipe[i]->y-perso[ed->perso_select]->taille*image.sprite_perso[ed->perso_select]->h;



set_cooldown_move_all(perso,ed->nb_persos_max, 10);
                  break;
                 }
             }


//         }
         }
     }

  }
  }





}




int arrivee(t_perso** perso, t_editeur* ed, btmp image, int x_arrivee)
{
    int i;
    if(ed->scroll_x+SCREEN_W>x_arrivee)
    {
        for(i=0;i<ed->nb_persos_max;i++)
        {
            if(perso[i]!=NULL)
            {
                if(ed->scroll_x+perso[i]->x+image.sprite_perso[i]->w>=x_arrivee)
                {

                    return 1;
                }
            }
        }
    }


return 0;
}

int check_arrivee(t_perso** perso,t_editeur* ed, btmp image, int check)
{
    int i,g;
     float r=ed->fps_ratio;
    if(check==1)
    {
       for(i=0;i<ed->nb_persos_max;i++)
       {
           if(perso[i]!=NULL)
           {
//                perso[i]->x-=r*ed->vit_x_max*5;
//                 perso[i]->y=SCREEN_H/2-SCREEN_H/60;
                if(ed->scroll_x<SCREEN_W)
                {


                  return 0;

                }
           }
       }
    }

return 1;
}



void life_check(t_perso** perso,t_editeur* ed,t_effect* effect)
{
    int i;
    for(i=0;i<ed->nb_persos_max;i++)
    {
        if(perso[i]!=NULL)
        {



//            perso[i]->life_max=100*perso[i]->taille;
if(perso[i]->life==perso[i]->life_max)
{


    if(perso[i]->etat<=1)
    etat_mario(perso, i, 1);
}

if(perso[i]->life<perso[i]->life_max)etat_mario(perso, i, 0);

             if((int)perso[i]->life_old>(int)perso[i]->life)
            {
               play_sample(effect->hurt[rand()%2],255,128,1000,0);
               perso[i]->life_old=perso[i]->life;
            }

        }
    }


}



void obj_sol(t_objet** bloc,BITMAP* sprite,BITMAP* buffer_obst,int ra,int i)
{
int h;
     if(bloc[i]->vit_y>0)bloc[i]->vit_y=0;


   for(h=sprite->h+2;h<sprite->h+4;h++)
   {
       if(getpixel(buffer_obst,bloc[i]->x+sprite->w/(2*ra) , bloc[i]->y+h )!=makecol(255,0,255))
       {
           bloc[i]->y+=sprite->h-h;
       }
   }





}
