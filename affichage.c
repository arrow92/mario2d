#include "fonctions.h"





btmp affiche_persos(btmp image, t_perso** perso, t_editeur* ed)
{
   int i;
int mario_type=0;
int sprite_nb=0;

   for(i=0;i<ed->nb_persos_max;i++)
   {
  if(perso[i]!=NULL)
  {
switch(perso[i]->etat)
{
    case 0: mario_type=0;break;
     case 1: mario_type=0;break;
      case 2: mario_type=1;break;
}

//switch(i)
//{
//    case 0:sprite_nb=0;break;
//    case 1:sprite_nb=2;break;
//    case 2:sprite_nb=4;break;
//    case 3:sprite_nb=6;break;
//    default: sprite_nb=0;break;
//}

image.sprite_perso[i]=animation_perso(image.sprite_perso[i],image.perso[mario_type+perso[i]->character],perso, ed, i);

//   draw_sprite(image.buffer_persos,image.sprite_perso[i],perso[i]->x,perso[i]->y);
if(ed->acc_y<0)
rotate_scaled_sprite_v_flip(image.buffer_persos,image.sprite_perso[i], perso[i]->x,perso[i]->y,0,ftofix(perso[i]->taille));

if(ed->acc_y>0)
    rotate_scaled_sprite(image.buffer_persos,image.sprite_perso[i], perso[i]->x,perso[i]->y,0,ftofix(perso[i]->taille));


  }


   }


    return image;
}




void sprite_change(t_perso** perso,int i, char k_change)
{
if(k_change)
{
if(perso[i]!=NULL)
{
    if(perso[i]->cooldown<=0)
    {
        set_cooldown(perso,i,100);
          perso[i]->character+=2;
   if(perso[i]->character>6)
    perso[i]->character=0;
    }


}
}

}

//btmp
btmp perso_idle(btmp image, t_perso** perso,int i)
{
   int dec_x=0;
   int dec_y=0;

if(perso[i]!=NULL)
{

 clear_to_color(image.sprite_perso[i],makecol(255,0,255));
    if(perso[i]->direction==1)dec_y=4;
     if(perso[i]->direction==-1)dec_y=0;

   image.sprite_perso[i]=blit_perso(image.sprite_perso[i], image.perso[0], dec_x,dec_y, perso[i]->direction);
}



    return image;
}

btmp border_draw(t_perso** perso, btmp image,t_editeur* ed)
{
blit(image.bckg, image.buffer_bckg, ed->scroll_x, 0, 0,0,SCREEN_W,SCREEN_H);

image=charge_obstacle_intel(image,image.buffer_obst, image.buffer_bckg,perso,ed,0,0 ,makecol(255,0,0), 1);
    return image;
}


btmp block(btmp image, t_editeur* ed, t_objet** bloc,BITMAP* block,int W,int H,int nb_max,int cote)
{
int i;
int j;
BITMAP* bk=create_bitmap(W,H);
for(i=0;i<nb_max;i++)
{
    if(bloc[i]!=NULL)
    {

                clear_to_color(bk,makecol(255,0,255));
        masked_blit(block,bk,bloc[i]->sprite_etat*bk->w, 0,0,0,bk->w,bk->h );
        if(bloc[i]->bonus==10)
        {
            for(j=0;j<5;j++)
            rect(bk,j,j,bk->w-j,bk->h-j,makecol(0,255,0));
        }
         if(bloc[i]->bonus==11)
        {
            for(j=0;j<5;j++)
            rect(bk,j,j,bk->w-j,bk->h-j,makecol(255,0,0));
        }

 if(bloc[i]->type==-2)
{
 draw_sprite(image.buffer, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y);
}

if(bloc[i]->type==-1)
{
 draw_sprite(image.buffer, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y);
}

if(bloc[i]->type==0)
image=charge_obstacle(image,image.buffer_obst, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y, makecol(255,0,0), 1);

if(bloc[i]->type==1)
{
if(cote==0)
draw_sprite(image.buffer, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y);

if(cote==1)
{
    if(bloc[i]->cs==1)
        draw_sprite(image.buffer, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y);

    if(bloc[i]->cs==-1)
         draw_sprite_h_flip(image.buffer, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y);
}


}


// textprintf_ex(image.buffer, font,bloc[i]->x-ed->scroll_x+bk->w/2, bloc[i]->y+bk->h/2, makecol(0, 0, 0),
//		    -1, "%d ",bloc[i]->bonus);

    }
}

destroy_bitmap(bk);
return image;
}


btmp antigravite(btmp image, t_editeur* ed, t_objet** bloc,BITMAP* block[2],int W,int H,int nb_max,int cote)
{
int i;
int j;
int number=0;
BITMAP* bk=create_bitmap(W,H);
for(i=0;i<nb_max;i++)
{
    if(bloc[i]!=NULL)
    {
if(bloc[i]->bonus==20)
{
    number=0;
}
if(bloc[i]->bonus==21)
{
    number=1;
}

clear_to_color(bk,makecol(255,0,255));
masked_blit(block[number],bk,bloc[i]->sprite_etat*bk->w, 0,0,0,bk->w,bk->h );
 draw_sprite(image.buffer, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y);
    }
}

destroy_bitmap(bk);
return image;
}


btmp block_static(btmp image, t_editeur* ed, t_objet** bloc,BITMAP* block,int nb_max,int c_map)
{
int i;
BITMAP* bk=create_bitmap(block->w/5,block->h);
for(i=0;i<nb_max;i++)
{
    if(bloc[i]!=NULL)
    {

                clear_to_color(bk,makecol(255,0,255));
        masked_blit(block,bk,bloc[i]->sprite_etat*bk->w, 0,0,0,bk->w,bk->h );

//                   image=charge_obstacle(image,image.background[c_map], bk,bloc[i]->x-ed->scroll_x, bloc[i]->y, makecol(255,0,0), 1);
draw_sprite(image.bckg, bk,bloc[i]->x-ed->scroll_x, bloc[i]->y);

bloc[i]=NULL;


    }
}

destroy_bitmap(bk);
return image;
}


BITMAP* blit_perso(BITMAP* sprite, BITMAP* source, int dec_x,int dec_y, int direction)
{

    if(direction==1)
      {
           BITMAP* buff=create_bitmap((source->w/4),(source->h/4) );
    clear_to_color(buff, makecol(255,0,255));
   masked_blit(source, buff,(source->w/4)*dec_x,(source->h/4)*dec_y,0,0,(source->w/4),178);
   draw_sprite_h_flip(sprite,buff,0,0);

     destroy_bitmap(buff);
      }
     if(direction==-1)
      {
           masked_blit(source, sprite,(source->w/4)*dec_x,(source->h/4)*dec_y,0,0,(source->w/4),178);
      }


return sprite;
}


BITMAP* animation_perso(BITMAP* sprite, BITMAP* source, t_perso** perso,t_editeur* ed, int i)
{
    int dec_x=0;
    int dec_y=0;

    if(perso[i]!=NULL)
    {



      if(perso[i]->direction==1)
      {
//        dec_y+=4;
      }

         if(perso[i]->idle==0) perso[i]->compteur_idle=0;
         if(perso[i]->contact==0)perso[i]->compteur_idle=0;

      if(perso[i]->compteur_idle>300)
{
   dec_y+=3;
}

   else{


         if(perso[i]->contact==1)
        {
            perso[i]->cooldown_sol=0;
          set_cooldown_sol(perso,i, 10);
        }

        if(perso[i]->contact==0)
        {

 if(perso[i]->cooldown_sol<=0)
            dec_y+=1;

        }


      if(perso[i]->idle==0)
      {

          perso[i]->compteur_idle=0;

          if(perso[i]->cooldown_sprite<=0)
          {
             perso[i]->sprite_etat++;

               if(perso[i]->sprite_etat>3)
      {
          perso[i]->sprite_etat=0;
      }
            set_cooldown_sprite(perso,i,8) ;
          }

      }


 if(perso[i]->idle==1)
      {
          perso[i]->sprite_etat=0;
          perso[i]->compteur_idle+=ed->fps_ratio;
      }

    }

       clear_to_color(sprite,makecol(255,0,255));



    sprite=blit_perso(sprite,source,perso[i]->sprite_etat,dec_y,perso[i]->direction) ;
    }
    return sprite;
}

void tout_afficher_screen(btmp image,t_editeur* ed,t_perso** perso,t_objet** bloc,t_objet** b_bloc,t_objet** para,t_objet** goom,t_objet** piece,t_objet** nuage,t_objet** boo,t_objet** pipe,t_objet** fireball,t_objet** antig)
{
    clear_to_color(image.buffer_obst,makecol(255,0,255));
    clear_to_color(image.buffer,makecol(255,0,255));
    clear_to_color(image.buffer_map,makecol(255,0,255));
     clear_to_color(image.buffer_persos,makecol(255,0,255));
      clear_to_color(image.buffer_enemi,makecol(255,0,255));
//      clear_to_color(image.buffer_obj,makecol(255,0,255));





    image.buffer=background_scroll(image,ed);
      image.buffer=moving_clouds(nuage, image.buffer,image.cloud,ed->scroll_x,ed->weather_max, ed->fps_ratio);
      image.buffer_enemi=affiche_boo(boo,ed->nb_boo_max,image.boo, image.buffer_obst);
      image=charge_obstacle_intel(image,image.buffer_obst, image.buffer_enemi,perso,ed,0,0 ,makecol(0,255,0), 0);

//image=charge_obstacle(image,image.buffer_obst, image.pipe_2,2*SCREEN_W-ed->scroll_x,SCREEN_H/2 ,makecol(0,100,0), 1);
//image=charge_obstacle(image,image.buffer_obst, image.pipe_3,2*SCREEN_W-ed->scroll_x,0 ,makecol(0,110,0), 1);


    image=border_draw(perso,image,ed);




image=affiche_pipes(pipe,ed, image,perso); //les tuyaux verts teleport
image=block(image,ed, piece,image.piece,image.piece->w/20,image.piece->h,ed->nb_pieces_max,0);

image=antigravite(image,ed, antig,image.antig,image.antig[0]->w/20,image.antig[0]->h,ed->nb_antig_max,0);

    image=block(image,ed, bloc,image.block,image.block->w/5,image.block->h,ed->nb_blocks_max,0);
     image=block(image,ed, b_bloc,image.b_block,image.b_block->w/5,image.b_block->h,ed->nb_b_blocks_max,0);

       image=block(image,ed, para,image.para,image.para->w/7,image.para->h,ed->nb_para_max,0);
 image=block(image,ed, goom,image.goom,image.goom->w/9,image.goom->h,ed->nb_goom_max,1);
 image=block(image,ed, pipe,image.pipe,image.pipe->w,image.pipe->h,ed->nb_pipes_max,0);

  image=block(image,ed, fireball,image.fireball,image.fireball->w/10,image.fireball->h,ed->nb_persos_max*ed->nb_fire_max,0);

    image.buffer=moving_clouds(boo, image.buffer,image.boo,ed->scroll_x,ed->nb_boo_max,ed->fps_ratio);


    draw_sprite(image.buffer_obj,image.buffer,0,0);

 if((ed->c_map%5==0)&&(ed->c_map!=0))cave(perso,ed,image.buffer,image.voile,image.sprite_perso[0],1.5 +(float)ed->pieces/10); //noir

//   image.buffer=pieces_afficher(image.piece,image.buffer,perso,ed,piece);


   if(ed->show_map==1) {show_map_advancement(image.buffer_map,ed->scroll_x, image.bckg->w);ed->show_map=0;
draw_sprite(image.buffer,image.buffer_map,SCREEN_W/2,SCREEN_H/2);}



   image=affiche_persos(image,perso, ed);

image.buffer=barre_de_vie(perso,ed, image.buffer);
image.buffer=afficher_nb_pieces(image.buffer, image.piece,ed->pieces);
if(ed->cursor==1)image.buffer=cursor(image.buffer,image.sprite_perso[0],perso,ed);
image.buffer=beta_coord(perso,ed,image.buffer);
//

  textprintf_ex(image.buffer, font,SCREEN_W/4,SCREEN_H/4, makecol(0, 0, 0),
		    -1, "%d",(int)ed->fps);



//draw_sprite(image.buffer,image.buffer_obst,0,0); //rrouge



    draw_sprite(image.buffer,image.buffer_persos,0,0);

    draw_sprite(screen,image.buffer,0,0);
}



btmp charge_obstacle_turned(btmp image,BITMAP* bmp, BITMAP* pic,BITMAP* temp, float x, float y,int h,int w,float rotation)
{
 int i,j;
 int c;
rotate_scaled_sprite(bmp,pic,x,y,ftofix(rotation),ftofix(1));
rotate_scaled_sprite(temp,pic,x,y,ftofix(rotation),ftofix(1));


 for(i=0;i<w;i++)
 {
     for(j=0;j<h;j++)
     {
        if(getpixel(temp,i,j)!=makecol(255,0,255))
        {
            putpixel(image.buffer_obst,i,j,makecol(255,0,0));

        }

     }

 }




 return image;
}


BITMAP* barre_de_vie(t_perso** perso,t_editeur* ed, BITMAP* buffer)
{
   int i;
   int x,y;
   int couleur;
   for(i=0;i<ed->nb_persos_max;i++)
   {
       if(perso[i]!=NULL)
       {
        switch(i)
        {
            case 0: x=SCREEN_W/30; y=SCREEN_H/40; break;
            case 1: x=SCREEN_W-SCREEN_W/30-SCREEN_W/4; y=SCREEN_H/40; break;
            case 2: x=SCREEN_W/30; y=3*SCREEN_H/40; break;
            case 3: x=SCREEN_W-SCREEN_W/30-SCREEN_W/4; y=3*SCREEN_H/40;break;
            case 4: x=SCREEN_W/30; y=5*SCREEN_H/40;break;
        }
        if(perso[i]->couleur!=makecol(100,100,100))
        buffer=barre(buffer,x,y,perso[i]->couleur, perso,i,ed);
        textprintf_ex(buffer, font,x +SCREEN_W/8-SCREEN_W/30,y+SCREEN_H/80, makecol(0, 0, 0),
		    -1, "%d / %d",(int)perso[i]->life, (int)perso[i]->life_max);
       }
   }
    return buffer;
}


BITMAP* barre(BITMAP* buffer,int x,int y, int couleur, t_perso** perso, int i, t_editeur* ed)
{

rectfill(buffer,x,y,x+((float)perso[i]->life/(float)perso[i]->life_max)*(float)(SCREEN_W/4),y+SCREEN_H/30, couleur);
rect(buffer,x,y,x+SCREEN_W/4,y+SCREEN_H/30,makecol(0,0,0));
    return buffer;
}

BITMAP* cursor(BITMAP* buffer,BITMAP* sprite,t_perso** perso, t_editeur* ed)
{
    int i;
    float width=SCREEN_H/60;
    float length=width;
    float radius=sprite->w/5;
    int g;
    int x,y;
    float rat=1;
    float rad=0.0174532925;
    int dehors=0;
    BITMAP* cercle=create_bitmap(3*radius,3*radius);


    for(i=0;i<ed->nb_persos_max;i++)
    {
        dehors=0;
        if(perso[i]!=NULL)
        {
            radius=sprite->w/5;
            x=(int)perso[i]->x;
y=(int)perso[i]->y;

if(perso[i]->x+sprite->w/2<0){dehors=1;}
if(perso[i]->x>SCREEN_W){dehors=2;}
if(dehors==1){x=0; }
if(dehors==2)x=SCREEN_W-3*radius;


            clear_to_color(cercle,makecol(255,0,255));
//            rectfill(buffer,perso[i]->x+sprite->w/2-length/2, perso[i]->y-1.5*width,perso[i]->x+sprite->w/2+length/2, perso[i]->y-0.5*width,  perso[i]->couleur);

if(dehors!=0)circlefill(cercle,cercle->w/2,cercle->h/2,1.5*radius,makecol(255,0,0));

       circlefill(cercle,cercle->w/2,cercle->h/2,radius,perso[i]->couleur);
       rat=perso[i]->life/perso[i]->life_max;
       rat=360*rat;
       rat=360-rat;
       for(g=0;g<rat;g++)
       {
           line_epaisse(cercle,cercle->w/2,cercle->h/2,cercle->w/2 +radius*cos(g*rad),cercle->h/2+ radius*sin(-g*rad),makecol(255,0,255));
       }



        draw_sprite(buffer,cercle,x+sprite->w/2-cercle->w/2,y-2.5*radius);

        }

    }

destroy_bitmap(cercle);
   return buffer;
}


void scrolling(t_editeur* ed,t_perso** perso,btmp image)
{
  int i;
  float x_max=0;
  int p=0;
  for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
       if(perso[i]->x>x_max){x_max=perso[i]->x;p=i;}
      }
  }
   if(x_max>SCREEN_W/2)
   { ed->scroll_x_old=ed->scroll_x;
if(perso[p]!=NULL)
{
   while(perso[p]->x+ed->scroll_x_old-ed->scroll_x>SCREEN_W/2)
   {
     ed->scroll_x++;
   }
}
//       ed->scroll_x=x_max+image.sprite_perso[i]->w/2-SCREEN_W/2;
 for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
     perso[i]->x+=ed->scroll_x_old-ed->scroll_x;
      }
  }


   }


    if(x_max<SCREEN_W/6)
   { ed->scroll_x_old=ed->scroll_x;
if(perso[p]!=NULL)
{
 while(perso[p]->x+ed->scroll_x_old-ed->scroll_x<SCREEN_W/6)
   {
     ed->scroll_x--;
   }
}


//       ed->scroll_x=x_max+image.sprite_perso[i]->w/2-SCREEN_W/2;
 for(i=0;i<ed->nb_persos_max;i++)
  {
      if(perso[i]!=NULL)
      {
     perso[i]->x+=ed->scroll_x_old-ed->scroll_x;
      }
  }

   }

}


BITMAP* background_scroll(btmp image, t_editeur* ed)
{
    int scroll=ed->scroll_x;
   int g=scroll/image.background[0]->w;
int x_2=g*image.background[0]->w-3*scroll/4;
int x=g*image.background[0]->w-2*scroll/4;

draw_sprite(image.buffer,image.background[0],x,0);
draw_sprite(image.buffer,image.background[0],x+image.background[0]->w,0);
draw_sprite(image.buffer,image.background[0],x-image.background[0]->w,0);
draw_sprite(image.buffer,image.background[0],x-2*image.background[0]->w,0);

draw_sprite(image.buffer,image.background[1],x_2,SCREEN_H-image.background[1]->h);
draw_sprite(image.buffer,image.background[1],x_2+image.background[0]->w,SCREEN_H-image.background[1]->h);
draw_sprite(image.buffer,image.background[1],x_2-image.background[0]->w,SCREEN_H-image.background[1]->h);

//if(g%2==1){draw_sprite_h_flip(image.buffer,image.background[0],x,0);
//draw_sprite(image.buffer,image.background[0],x+image.background[0]->w,0);
//}

    return image.buffer;
}


BITMAP* pieces_afficher(BITMAP* piecee, BITMAP* buffer,t_perso** perso,t_editeur* ed,t_objet** piece)
{
    int i;
    int pieces_tot=0;

    for(i=0;i<ed->nb_persos_max;i++)
    {
        if(perso[i]!=NULL)
        {
          pieces_tot+=perso[i]->pieces;
        }
    }

    for(i=0;i<ed->nb_pieces_max;i++)
    {
        if(piece[i]!=NULL)
        {
//            draw_sprite(screen,piecee,piece[i]->x-ed->scroll_x,piece[i]->y);
           masked_blit(piecee,buffer,piece[i]->sprite_etat*((float)piecee->w/20),0,piece[i]->x-ed->scroll_x,piece[i]->y,(float)piecee->w/20,piecee->h );
        }
    }



    return buffer;
}


void sprite_next_piece(t_objet** piece, t_editeur* ed,int nb_max,int compteur_max, int s_max)
{
   int i;
   for(i=0;i<nb_max;i++)
   {
       if(piece[i]!=NULL)
       {
           piece[i]->compteur-=ed->fps_ratio;
           if(piece[i]->compteur<=0)
           {
               piece[i]->compteur=compteur_max;
               piece[i]->sprite_etat++;
               if(piece[i]->sprite_etat>s_max)
               {
                   piece[i]->sprite_etat=0;
               }
           }

//circlefill(screen,piece[i]->x-ed->scroll_x,piece[i]->y,20,makecol(i*20,0,0));

       }

   }



}


BITMAP* beta_coord(t_perso** perso,t_editeur* ed, BITMAP* buffer)
{
    int i;
if(perso[ed->perso_select]!=NULL)
  textprintf_ex(buffer, font,SCREEN_W/6,SCREEN_H/6, makecol(0, 0, 0),
		    -1, "%d    %d   %d",(int)ed->fps, mouse_x+(int)ed->scroll_x,mouse_y);

    return buffer;
}


BITMAP* afficher_nb_pieces(BITMAP* buffer, BITMAP* image, int pieces)
{
    masked_blit(image,buffer,0,0,SCREEN_W-4*image->w/20,2*image->h,image->w/20,image->h);
//    draw_sprite(buffer,image,);
     textprintf_ex(buffer, font, SCREEN_W-3*image->w/20,3*image->h, makecol(0, 0, 0),
		    -1, "%d", pieces);

    return buffer;
}


BITMAP* moving_clouds(t_objet** nuage, BITMAP* buffer,BITMAP* cloud[2],float scroll_x, int nb, float r)
{
    int i;
    int g=0;

    for(i=0;i<nb;i++)
    {
        if(nuage[i]!=NULL)
        {
            if(nuage[i]->interactif==0)
            {
          nuage[i]->x=-scroll_x+nuage[i]->compteur ;

nuage[i]->compteur-=r*nuage[i]->vit_x/100;
nuage[i]->cs-=r*nuage[i]->vit_y*0.0001;
if(nuage[i]->cs<=0)nuage[i]->cs=360;
        if(nuage[i]->compteur+cloud[g]->w<=scroll_x)nuage[i]->compteur=SCREEN_W+cloud[g]->w+scroll_x;

        nuage[i]->y=3*cloud[g]->h+cloud[g]->h*cos(nuage[i]->cs);
        }

        if(nuage[i]->interactif==1)
        {
       if(pow(nuage[i]->cote,2)==4)
       {
nuage[i]->x=-scroll_x+nuage[i]->compteur ;

       }

       else{
//            g=0;

        nuage[i]->x=-scroll_x+nuage[i]->compteur ;

nuage[i]->compteur+=r*nuage[i]->cote*nuage[i]->vit_x/100;
nuage[i]->cs-=r*nuage[i]->vit_y*0.0001;
if(nuage[i]->cs<=0)nuage[i]->cs=360;
        if(nuage[i]->compteur+cloud[g]->w<=scroll_x)nuage[i]->compteur=SCREEN_W+cloud[g]->w+scroll_x;

        nuage[i]->y+=nuage[i]->y_cot*nuage[i]->vit_y/100;
       }


        }
        }

    }


buffer=affiche_boo(nuage,nb,cloud, buffer);



   return buffer;
}


BITMAP* affiche_boo(t_objet** nuage,int nb, BITMAP* cloud[5], BITMAP* buffer)
{
    int i;
    int g=0;

     for(i=0;i<nb;i++)
    {
        if(nuage[i]!=NULL)
        {
if(nuage[i]->cote==1)g=1;
if(nuage[i]->cote==-1)g=0;

if(nuage[i]->cote==2)g=3;
if(nuage[i]->cote==-2)g=2;

    draw_sprite(buffer,cloud[g],nuage[i]->x,nuage[i]->y);
//    printf("%d\n", g);
        }
    }


    return buffer;
}



btmp affiche_pipes(t_objet** pipe,t_editeur* ed,btmp image, t_perso** perso)
{

  int i;
  for(i=0;i<ed->nb_pipes_max;i++)
  {
      if(pipe[i]!=NULL)
      {
          if((pipe[i]->x-ed->scroll_x>0)&&(pipe[i]->x-ed->scroll_x<7*SCREEN_W/6))
          {

         image=charge_obstacle(image,image.buffer_obst,image.pipe,pipe[i]->x-ed->scroll_x,pipe[i]->y,makecol(255,0,0), 1);
          }

           if(pipe[i]->x_2-ed->scroll_x>0&&pipe[i]->x_2-ed->scroll_x<7*SCREEN_W/6)
          {
        image=charge_obstacle(image,image.buffer_obst,image.pipe,pipe[i]->x_2-ed->scroll_x,pipe[i]->y_2,makecol(255,0,0), 1);
          }
      }
  }

  return image;
}





void camera_clic_move(t_editeur* ed, BITMAP* mape)
{
  show_mouse(screen);

  if(mouse_b&1)
  {
      ed->scroll_x=((float)mouse_x/(float)SCREEN_W)*(mape->w-SCREEN_W);
      ed->show_map=1;
//     do{rest(1);} while(mouse_b&1);
  }

}



void show_map_advancement(BITMAP* buffer,float scroll, int width)
{
    float g=7;
    float W=(float)SCREEN_W;
     float H=(float)SCREEN_H;
     float ep=20;



     float wdth=W/g;

      float w_vue=W/(width);
     float hgt=H/g;
     float sc_f=(float)(scroll/(float)width);

    rectfill(buffer, 0,0,wdth-wdth/ep,hgt, makecol(255,0,0)  );
    rectfill(buffer, 0+wdth/ep,0+hgt/ep,wdth-2*wdth/ep,hgt-hgt/ep, makecol(255,0,255)  );

    rectfill(buffer, 0+2*wdth/ep+sc_f*wdth,0+hgt/ep,wdth/ep+sc_f*wdth+w_vue,hgt-hgt/ep, makecol(0,0,255)  );


}



void chgmt_map(btmp image,t_perso** perso,t_editeur* ed,t_objet** pipe,t_objet** bloc,t_objet** nuage,t_objet** boo,t_objet** piece)
{
    int i;
    for(i=0;i<ed->nb_persos_max;i++)
    {
        free(perso[i]);
    }

  for(i=0;i<ed->nb_pipes_max;i++)
  {
      free(pipe[i]);
  }

    for(i=0;i<ed->nb_blocks_max;i++)
    {
        free(bloc[i]);
    }

    for(i=0;i<ed->weather_max;i++)
    {
        free(nuage[i]);
    }

    for(i=0;i<ed->nb_pieces_max;i++)
    {
        free(piece[i]);
    }

    free(ed);

}


void carreaux(int x,int y,int W, int H)
{
//    BITMAP* c=create_bitmap(SCREEN_W,SCREEN_H);
//    clear_to_color(c,makecol(255,0,255));
    int i;
    int r_x=(x/W)*W;
    int r_y=(y/H)*H;
    for(i=0;i<SCREEN_W;i+=W)
    {
       line_epaisse(screen,i,0,i,SCREEN_H,makecol(255,255,255));
    }

    for(i=0;i<SCREEN_H;i+=H)
    {
       line_epaisse(screen,0,i,SCREEN_W,i,makecol(255,255,255));
    }

    rectfill(screen,r_x,r_y,r_x+W,r_y+H,makecol(0,255,0));

//    destroy
}


BITMAP* multicolor(BITMAP* pic)
{
    int i,j;
    int c;
    for(i=0;i<pic->w;i++)
    {
        for(j=0;j<pic->h;j++)
        {
            c=getpixel(pic,i,j);
            if(c!=makecol(255,0,255))
            {
                putpixel(pic,i,j,change_color(c,2));
            }
        }
    }




    return pic;
}


int change_color(int c, int brightness)
{
    int r=getr(c);
    int v=getg(c);
    int b=getb(c);

    r+=brightness;
    v+=brightness;
    b+=brightness;

r=c_add(r);
v=c_add(v);
b=c_add(b);

   return makecol(r,v,b);
}

int c_add(int couleur)
{
    if(couleur<0)
    {
        couleur=255;
    }
    else if(couleur>255)
    {
        couleur=0;
    }
    return couleur;
}


void cave(t_perso** perso,t_editeur* ed,BITMAP* buffer,BITMAP* voile,BITMAP* p,float radius)
{
int i;
clear_to_color(voile, makecol(0,0,0));
for(i=0;i<ed->nb_persos_max;i++)
{
    if(perso[i]!=NULL)
    {
      circlefill(voile,perso[i]->x+perso[i]->taille*p->w/2,perso[i]->y+perso[i]->taille*p->h/2,radius*perso[i]->taille*p->h,makecol(255,0,255));
    }
}

draw_sprite(buffer, voile,0,0);

}

BITMAP* display_finish(BITMAP* finish,BITMAP* sprite,BITMAP* buffer, t_objet** bloc,int nb_max, int x_arrivee)
{
   float x=x_arrivee;
   float y=SCREEN_H-finish->h;
   int sens=1;

int i;
for(i=0;i<nb_max;i++)
{
    if(bloc[i]!=NULL)
    {
      if(bloc[i]->x+3*sprite->w>x_arrivee&&bloc[i]->x<x_arrivee)
      {
          x=bloc[i]->x;

          if(bloc[i]->y<SCREEN_H/3)
          {
              sens=-1;
          y=bloc[i]->y+sprite->h;
          }
          else{y=bloc[i]->y-finish->h;sens=1;}

      }
    }
}
switch(sens)
{
    case 1:draw_sprite(buffer,finish,x,y);break;
    case -1:draw_sprite_v_flip(buffer,finish,x,y);break;
}

 return buffer;
}
